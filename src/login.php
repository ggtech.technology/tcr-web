<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <style>
        *{
            font-family: 'Kodchasan', sans-serif;
        }
        .swal2-title {
            font-family: 'Kodchasan', sans-serif;
        }
    </style>
</head>
<body>
<?php
    $email=$_POST["email"];
    $pass=$_POST["password"];

    httpPost('http://203.150.243.105/serve/login/auth', array('email' => $email,'password' => $pass));

    function httpPost($url,$params)
    {
        session_start();
        require("conn.php");

        $postData = http_build_query($params);
        $curl_int = curl_init();  
     
        curl_setopt($curl_int,CURLOPT_URL,$url);
        curl_setopt($curl_int,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl_int,CURLOPT_HEADER, false); 
        curl_setopt($curl_int, CURLOPT_POST, $postData);
        curl_setopt($curl_int, CURLOPT_POSTFIELDS, $postData);    
    
        $output=curl_exec($curl_int);
       # $dec = json_encode($output);
        curl_close($curl_int);
        $obj = json_decode($output);
        $email = $obj->email;
        $token = $obj->token;
        $role = $obj->role;
        $sta = $obj->status_get;
        
        // $user="";

        $sql = "SELECT * FROM thj_account WHERE thj_acc_email_user ='$email'";
        $result = mysqli_query($conn , $sql);

        $excq = $conn -> query($sql);
        $row = $excq -> fetch_assoc();

        if($email==null&&$token==null){
            echo "<script>";
            echo "Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'เข้าสู่ระบบไม่สำเร็จ',
                showConfirmButton: false,
                timer: 1500
                })";
            header("Refresh:1.5; url=../index.php");
            echo "</script>";
            exit();
        }
        else{
            // echo $row["thj_acc_status"];
            // print $email."<br>".$token."<br>".$sta;
            if($sta == 1){
                // print $email."<br>".$token."<br>".$sta;
                $_SESSION['thj_acc_email_user']=$email;
                echo "<script type=\"text/javascript\">";
                echo "Swal.fire({
                    // position: 'top-end',
                    icon: 'success',
                    title: 'เข้าสู่ระบบสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                    })";
                header("Refresh:1.5; url=../admin.php"); 
                echo "</script>";
                
                exit();
            }
            elseif($sta == 2){
                // print $email."<br>".$token."<br>".$sta;
                $_SESSION['thj_acc_email_user']=$email;
                echo "<script type=\"text/javascript\">";
                echo "Swal.fire({
                    // position: 'top-end',
                    icon: 'success',
                    title: 'เข้าสู่ระบบสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                    })";
                header("Refresh:1.5; url=../major.php");
                echo "</script>";
                
                exit();
            }
            else{
                echo "<script>";
                echo "Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'เข้าสู่ระบบไม่สำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                    })";
                header("Refresh:1.5; url=../index.php");
                echo "</script>";
                exit();
            }
            
        }
        
        
    }
?>
</body>
</html>