var express = require('express');
var router = express.Router();
var db = require('../condb');
const jwt = require('jsonwebtoken');
var md5 = require("md5");
var multer = require('multer');
var path = require('path');
const fs = require('fs');

let external_func = require('./external_func');
$host = external_func.host_now();
const { Console } = require('console');
const img_acc_storage = multer.diskStorage({

    destination: function (req, file, cb) {
        cb(null, 'img_ACC/')
    },
    filename: function (req, file, cb) {
        cb(null, "THC_ACC_IMG-" + Date.now() + path.extname(file.originalname));
    }
})
const img_acc_upload = multer({
    storage: img_acc_storage,
    fileFilter: function (req, file, cb) {
        external_func.checkFileType(file, cb);
    }
});


const vip_proof_storage = multer.diskStorage({

    destination: function (req, file, cb) {
        cb(null, 'IMG_VIP_PROOF/')
    },
    filename: function (req, file, cb) {
        cb(null, "IMG_VIP_PROOF-" + Date.now() + path.extname(file.originalname));
    }
})
const vip_proof_upload = multer({
    storage: vip_proof_storage,
    fileFilter: function (req, file, cb) {
        external_func.checkFileType(file, cb);
    }
});


const seller_storage = multer.diskStorage({

    destination: function (req, file, cb) {
        cb(null, 'img_seller/')
    },
    filename: function (req, file, cb) {
        cb(null, "IMG_seller-" + Date.now() + path.extname(file.originalname));
    }
})
const seller_img_upload = multer({
    storage: seller_storage,
    fileFilter: function (req, file, cb) {
        external_func.checkFileType(file, cb);
    }
});



const seller_proof_storage = multer.diskStorage({

    destination: function (req, file, cb) {
        cb(null, 'IMG_seller_proof/')
    },
    filename: function (req, file, cb) {
        cb(null, "IMG_seller-PROOF" + Date.now() + path.extname(file.originalname));
    }
})
const seller_proof_upload = multer({
    storage: seller_proof_storage,
    fileFilter: function (req, file, cb) {
        external_func.checkFileType(file, cb);
    }
});


//------------img-user
router.post('/user_img', img_acc_upload.single('image'), async function (req, res) {
    const { user_id } = req.body

    let path = 'http://' + $host + '/all_image/img_ACC/' + req.file.filename;
    var sql = `INSERT INTO thj_image_user (thj_image_user_path, thj_image_user_user) VALUES(?)`;
    var values = [
        path, user_id];
    db.query(sql, [values], function (error, results, fields) {
        if (error) throw error;
        if (results.affectedRows > 0) {
            res.status(200).send(true);

        } else {
            res.status(200).send(false);
        }
    });
})
//-------------------------------------------------register------------------------------------------------
router.post('/register_acc_by_owner', function (req, res) {
    const { name, email_user, password_text, status_ac, id_card, banking_name, banking_number, owner_idx } = req.body
    var code_saler = external_func.makeid(10, owner_idx);
    //console.log("code " + email_user);
    var password = external_func.A_hash_passwd_sec(password_text)
    var getmail = `SELECT * FROM thj_account WHERE thj_acc_email_user = ?`
    var point = "0";
    db.query(getmail, email_user, function (error, result, fields) {
        if (error) throw error;
        numRows = result.length;
        if (numRows > 0) {
            //console.log("User Exist");
            res.status(400).send(false);
        } else {
            var sql = `insert into thj_account (thj_acc_id_point, thj_acc_name, thj_acc_email_user, thj_acc_password, thj_acc_status, 
                thj_acc_id_card, thj_acc_banking_name, thj_acc_banking_number, thj_acc_code_saler) values(?)`;
            var values = [
                point, name, email_user, password, parseInt(status_ac), id_card, banking_name, banking_number, code_saler];
            db.query(sql, [values], function (error, results, fields) {
                if (error) throw error;
                if (results.affectedRows > 0) {
                    res.status(200).send(true);
                } else {
                    res.status(200).send(false);
                }
            });

        }
    });
})

// router.post('/register_ac', async function (req, res) {
//     const { name, email_user, password_text, status_ac, id_card, banking_name, banking_number } = req.body
//     var code_saler = "-";
//     var json_str = password_text;
//     var password = external_func.A_hash_passwd_sec(json_str);
//     var getmail = `SELECT * FROM thj_account WHERE thj_acc_email_user = ?`
//     var point = "0";
//     db.query(getmail, email_user, function (error, result, fields) {
//         if (error) throw error;
//         numRows = result.length;
//         if (numRows > 0) {
//             //console.log(numRows);
//             res.status(400).send(false);
//         } else {
//             var sql = `insert into thj_account (thj_acc_id_point, thj_acc_name, thj_acc_email_user, thj_acc_password, thj_acc_status, 
//                 thj_acc_id_card, thj_acc_banking_name, thj_acc_banking_number, thj_acc_code_saler) values(?)`;
//             var values = [
//                 point, name, email_user, password, parseInt(status_ac), id_card, banking_name, banking_number, code_saler];
//             db.query(sql, [values], function (error, results, fields) {
//                 if (error) throw error;
//                 if (results.affectedRows > 0) {
//                     res.status(200).send(true);
//                 } else {
//                     res.status(200).send(false);
//                 }
//             });
//         }
//     });
// })

router.post('/register_ac', async function (req, res) {
    const { name, email_user, password_text, status_ac, id_card, banking_name, banking_number } = req.body
    var code_saler = "-";
    const json_str = password_text;
    const password = external_func.A_hash_passwd_sec(json_str);
    const userid_hashed = external_func.rand_idUser(10)+external_func.rand_idUser(5)
    //console.log(userid_hashed)
    var getmail = `SELECT * FROM thj_account WHERE thj_acc_email_user = ?`
    var point = "0";
    db.query(getmail, email_user, function (error, result, fields) {
        if (error) throw error;
        numRows = result.length;
        if (numRows > 0) {
            //console.log(numRows);
            res.status(400).send(false);
        } else {
            var sql = `insert into thj_account (thj_acc_id,thj_acc_id_point, thj_acc_name, thj_acc_email_user, thj_acc_password, thj_acc_status, 
                thj_acc_id_card, thj_acc_banking_name, thj_acc_banking_number, thj_acc_code_saler) values(?)`;
            var values = [userid_hashed,
                point, name, email_user, password, parseInt(status_ac), id_card, banking_name, banking_number, code_saler];
            db.query(sql, [values], function (error, results, fields) {
                if (error) throw error;
                if (results.affectedRows > 0) {
                    res.status(200).send(true);
                } else {
                    res.status(200).send(false);
                }
            });
        }
    });
})


// router.post('/register_seller', seller_img_upload.single('image'), async function (req, res) {
//     const { name, email_user, password_text, banking_name, banking_number } = req.body
//     var status_ac = "6"
//     var code_saler = "-";
//     let path_idcard = 'http://' +  $host + '/all_image/img_seller/' + req.file.filename;
//     //let path_idcard = 'http://localhost:4141/IMG_seller/' + req.file.filename;
//     var json_str = password_text;
   
//     var password = external_func.A_hash_passwd_sec(json_str);
//     var getmail = `SELECT * FROM thj_account WHERE thj_acc_email_user = ?`
//     var point = "0";
//     db.query(getmail, email_user, function (error, result, fields) {
//         if (error) throw error;
//         numRows = result.length;
//         if (numRows > 0) {
//             //console.log(numRows);
//             res.status(400).send(false);
//         } else {
//             var sql = `insert into thj_account (thj_acc_id,thj_acc_id_point, thj_acc_name, thj_acc_email_user, thj_acc_password, thj_acc_status, 
//                 thj_acc_id_card, thj_acc_banking_name, thj_acc_banking_number, thj_acc_code_saler) values(?)`;
//             var values = [
//                 point, name, email_user, password, parseInt(status_ac), path_idcard, banking_name, banking_number, code_saler];
//             db.query(sql, [values], function (error, results, fields) {
//                 if (error) throw error;
//                 if (results.affectedRows > 0) {
//                     res.status(200).send(true);
//                 } else {
//                     res.status(200).send(false);
//                 }
//             });
//         }
//     });
// })

router.post('/register_seller', seller_img_upload.single('image'), async function (req, res) {
    const { name, email_user, password_text, banking_name, banking_number } = req.body
    var status_ac = "6"
    var code_saler = "-";
    let path_idcard = 'http://' +  $host + '/all_image/img_seller/' + req.file.filename;
    //let path_idcard = 'http://localhost:4141/IMG_seller/' + req.file.filename;
    var json_str = password_text;
    const userid_hashed = external_func.rand_idUser(10)+external_func.rand_idUser(5)
    var password = external_func.A_hash_passwd_sec(json_str);
    var getmail = `SELECT * FROM thj_account WHERE thj_acc_email_user = ?`
    var point = "0";
    db.query(getmail, email_user, function (error, result, fields) {
        if (error){
            res.send(error)
        }
        numRows = result.length;
        if (numRows > 0) {
            //console.log(numRows);
            res.status(400).send(false);
        } else {
            var sql = `insert into thj_account (thj_acc_id,thj_acc_id_point, thj_acc_name, thj_acc_email_user, thj_acc_password, thj_acc_status, 
                thj_acc_id_card, thj_acc_banking_name, thj_acc_banking_number, thj_acc_code_saler) values(?)`;
            var values = [userid_hashed,
                point, name, email_user, password, parseInt(status_ac), path_idcard, banking_name, banking_number, code_saler];
            db.query(sql, [values], function (error, results, fields) {
                if (error){
                    res.send(error)
                }
                if (results.affectedRows > 0) {
                    res.status(200).send(true);
                } else {
                    res.status(200).send(false);
                }
            });
        }
    });
})

router.post('/seller_proof', seller_proof_upload.single('image'), async function (req, res) {
    const { request_user } = req.body;
    var request_date = external_func.date_regis();
    let path_proof = 'http://' +  $host + '/all_image/IMG_seller_proof/' + req.file.filename;
    var val = [request_user, path_proof, request_date]
    var seller_proof_sql = ` 
    INSERT INTO thj_seller_request (thj_seller_request_user, thj_seller_request_proof, thj_seller_request_date) VALUES (?) `;
    db.query(seller_proof_sql, [val], (err, result) => {
        if (err){
            res.send(err)
        }
        else if (result) {
            var data = result
            res.status(200).send(true);
        }
    })
});

router.post('/select_seller_request', function (req, res) {
    var select_seller_request = ` 
    SELECT 
thj_seller_request.thj_seller_request_id as req_id , 
thj_seller_request_user as acc_id,
thj_account.thj_acc_email_user as acc_user,
thj_seller_request.thj_seller_request_st as req_status,
thj_seller_request.thj_seller_request_date as req_date,
thj_seller_request.thj_seller_request_proof as req_proof
FROM thj_seller_request
LEFT JOIN thj_account ON thj_account.thj_acc_id = thj_seller_request.thj_seller_request_user`;
    db.query(select_seller_request, (err, result) => {
        if (err) {
            res.send(err)
        }
        else if (result) {
            var data = result
            res.send(data)
        }
    })
});


router.post('/seller_approve', function (req, res) {
    const { req_id, acc_id } = req.body;
    var seller_approve = ` 
    UPDATE thj_seller_request SET thj_seller_request_st = 1 WHERE thj_seller_request.thj_seller_request_id = ?`;
    db.query(seller_approve, req_id, (err, result) => {
        if (err) {
            res.json({ message: err })
        }
        else if (result) {
            var acc_update_role = ` 
            UPDATE thj_account SET thj_acc_status = 3 WHERE thj_account.thj_acc_id = ?`;
            db.query(acc_update_role, acc_id, (err, result) => {
                if (err) {
                    res.json({ message: err })
                }
                if (result.affectedRows > 0) {
                    res.writeHead(302, {
                        'Location': 'http://thr-online.com/tcr/admin.php?page=point'
                    });
                    res.end();
                }
            })
        }
    })
});

router.post('/select_user_point', function (req, res) {
    const { acc_id } = req.body;
    var order_onMajor = ` 
    SELECT thj_account.thj_acc_name as acc_name , thj_account.thj_acc_id_point as acc_point
    FROM thj_account WHERE thj_account.thj_acc_id = ? `;
    db.query(order_onMajor, acc_id, (err, result) => {
        if (err) {
            res.json({ message: err })
        }
        else if (result) {
            var data = result
            res.send(data)
        }
    })
});
//----------------------------------------------------------------------------------------------------------

//--------------------------------------------product --------------------------------------------------------------


router.post('/request_to_vip', vip_proof_upload.single('vip_proof'), async function (req, res) {
    const { acc_id } = req.body;
    var date_Ex = "-";
    var date_regis =  external_func.date_regis();


    let path = 'http://' + $host + '/all_image/IMG_VIP_PROOF/' + req.file.filename;
    //console.log(path);
    var check_vip = `SELECT thj_vip_proof.thj_vip_proof_id as vip_id FROM thj_vip_proof WHERE thj_vip_proof.thj_vip_proof_user_id = ?`;
    db.query(check_vip, acc_id, function (error, results, fields) {
        var ch_value = results[0]
        if (error) throw error;
        else if (ch_value) {

            var file_path = "IMG_VIP_PROOF/" + req.file.filename;
            external_func.del_file(file_path);

            res.status(200).send(false);
        } else {
            var sql = `INSERT INTO thj_vip_proof (thj_vip_proof_user_id,thj_vip_proof_date, thj_vip_proof_img,thj_vip_proof_dEx) VALUES(?)`;
            var values = [
                acc_id, date_regis, path, date_Ex];
            db.query(sql, [values], async function (error, results, fields) {
                if (error) {
                    console.log(error);
                }
                if (results) {
                    res.status(200).send(true);
                } else {
                    res.status(200).send(false);
                }
            });
        }
    });
}

);

router.get('/show_request_to_vip', async function (req, res) {
    var show_request_to_vip = `SELECT thj_vip_proof.thj_vip_proof_id as reqest_id,
    thj_vip_proof.thj_vip_proof_user_id as vip_userId_request ,
        thj_vip_proof.thj_vip_proof_img as vip_user_proof,
        thj_vip_proof.thj_vip_proof_status as vip_user_status,
        thj_vip_proof.thj_vip_proof_date as vip_request_date
        FROM thj_vip_proof
    `;
    db.query(show_request_to_vip, (err, result) => {
        if (err) {
            res.json({ message: err })
        }
        else if (result) {
            res.status(200).send(result);
        }
        else {
            res.status(200).send(false);
        }
    })
});




router.post('/upgrade_to_vip', async function (req, res) {
    const { acc_id, reqest_id } = req.body;
    // console.log(acc_id)
    var upgrade_to_vip = ` UPDATE thj_account SET thj_acc_status = 5 WHERE thj_account.thj_acc_id = ?`;
    db.query(upgrade_to_vip, acc_id, (err, result) => {
        if (err) {
            res.json({ message: err })
        }
        else if (result) {
            //res.status(200).send(true);
            var vip_status_done = ` UPDATE thj_vip_proof SET thj_vip_proof_status = 'vip' WHERE thj_vip_proof.thj_vip_proof_id = ?`;
            db.query(vip_status_done, reqest_id, (err, result) => {
                if (err) {
                    res.json({ message: err })
                }
                else if (result) {

                    var Expried_date_submit = external_func.date_expr();
                    //console.log(value);
                    var set_ExPried_date = `UPDATE thj_vip_proof SET thj_vip_proof_dEx = ? WHERE thj_vip_proof.thj_vip_proof_id = ?`;
                    db.query(set_ExPried_date, [Expried_date_submit, reqest_id], (err, result) => {
                        if (err) {
                            res.json({ message: err })
                        }
                        else if (result) {
                            res.writeHead(302, {
                                'Location': 'http://thr-online.com/tcr/admin.php?page=member'
                            });
                            res.end();
                        }
                        else {
                            res.status(200).send(false);
                        }
                    })
                }
                else {
                    res.status(200).send(false);
                }
            })
        }
        else {
            res.status(200).send(false);
        }
    })
});

router.post('/get_expried_date_vip', async function (req, res) {
    const { acc_id } = req.body;
    var get_last_vip_user = `SELECT 
    MAX(thj_vip_proof.thj_vip_proof_id) as vip_id
    FROM thj_vip_proof 
WHERE thj_vip_proof.thj_vip_proof_user_id=? AND thj_vip_proof_status='vip'
    `;
    db.query(get_last_vip_user, acc_id, (err, results) => {
        //console.log(results[0].vip_id)
        if (results) {
            let data_0 = results[0]

            var Proof_id = data_0['vip_id']
            //console.log(Proof_id)
            var get_date_last_vip_user = `
            SELECT 
            thj_vip_proof.thj_vip_proof_date as vip_User_date,
            thj_vip_proof.thj_vip_proof_dEx as vip_user_dEx
            FROM thj_vip_proof
            WHERE thj_vip_proof.thj_vip_proof_id = ? and thj_vip_proof.thj_vip_proof_status='vip' `;
            db.query(get_date_last_vip_user, Proof_id, (err, result) => {
                var ch_value = results[0]
                //console.log("bbb",results)
                if (ch_value) {
                    var regis_date = result[0].vip_User_date
                    var Ex_date = result[0].vip_user_dEx
                    var ch_date = external_func.vip_verify(regis_date, Ex_date)
                    res.status(200).json({ remain: ch_date });
                }
                else {
                    res.status(200).json({ message: "Account id " + acc_id + " _is not VIP" })
                }
            })
        }
    })
});

router.post('/downgrade_vip_user', async function (req, res) {
    var get_date_now = external_func.date_now();
    //console.log(get_date_now);
    var get_vip_user_Expried = `
    SELECT  thj_vip_proof.thj_vip_proof_id AS vip_id,
thj_vip_proof.thj_vip_proof_user_id as user_id
FROM thj_vip_proof 
WHERE thj_vip_proof.thj_vip_proof_dEx =?
    `;
    db.query(get_vip_user_Expried, get_date_now, (err, results) => {
        if (err) {
            res.json({ message: err })
        }
        else if (results) {
            var vip_id2 = [];
            var vip_id3 = [];
            var data = results

            for (let index9 = 0; index9 < results.length; index9++) {
                vip_id2.push(data[index9].user_id)
                vip_id3.push(data[index9].vip_id)
            }
            var downgrade_user = `UPDATE thj_account SET thj_acc_status = 4 WHERE thj_account.thj_acc_id IN(?) `;
            db.query(downgrade_user, [vip_id2], (err, result) => {
                //console.log(result);
                if (result) {
                    var downgrade_vip = `UPDATE thj_vip_proof SET thj_vip_proof_status = 'expried' WHERE thj_vip_proof.thj_vip_proof_id IN(?)`;
                    db.query(downgrade_vip, [vip_id3], (err, result) => {
                        //console.log(result);
                        if (result) {
                            res.send(true);
                        }
                        else {
                            res.send(false);
                        }
                    })
                }
                else {
                    res.send(false);
                }
            })

        }
    })
});


router.post('/renew_date_vip', vip_proof_upload.single('vip_proof'), async function (req, res) {
    const { acc_id } = req.body
    //console.log(acc_id);
    var get_vip = `SELECT thj_vip_proof.thj_vip_proof_id AS vip_id,
    thj_vip_proof.thj_vip_proof_dEx as date_expr
    FROM thj_vip_proof
    LEFT JOIN thj_account ON thj_account.thj_acc_id = thj_vip_proof.thj_vip_proof_user_id
    WHERE thj_account.thj_acc_id = ?
    `;
    db.query(get_vip, acc_id, (err, results) => {
        var res1_ch = results[0].date_expr

        if (err) {
            res.status(200).json({ message: acc_id + "Status is Request to be VIP" })
        }
        else if (res1_ch == "-") {
            var file_path = "IMG_VIP_PROOF/" + req.file.filename;
            external_func.del_file(file_path);
            res.status(200).send(false)
        }
        else {
            let path = 'http://' + $host + '/all_image/IMG_VIP_PROOF/' + req.file.filename;
            var data = results[0].vip_id;
            var get_date_db = results[0].date_expr
            var renew_date = external_func.date_renew(get_date_db)
            var status_x = "vip"
            var renew_vip = `UPDATE thj_vip_proof SET thj_vip_proof_img = ?,thj_vip_proof_dEx = ? , thj_vip_proof_status = ? WHERE thj_vip_proof.thj_vip_proof_id = ?`
            db.query(renew_vip, [path, renew_date, status_x, data], (err, results) => {
                var ch_value = results[0]
                if (err) {
                    res.status(200).json({ message: err })
                }
                if (results) {
                    var update_acc_status = `UPDATE thj_account SET thj_acc_status = 5 WHERE thj_account.thj_acc_id = ?`
                    db.query(update_acc_status, acc_id, (err, results) => {
                        if (err) {
                            res.status(200).json({ message: err })
                        }
                        if (results) {
                            res.status(200).json({ message: "Renew done" })
                        }
                        else {
                            res.status(200).json({ message: "Renew fail" })
                        }
                    })
                }
                else {
                    var file_path = "IMG_VIP_PROOF/" + req.file.filename;
                    external_func.del_file(file_path);
                    res.status(200).json({ message: "Not VIP" })
                }
            })
        }
    })
});


router.post('/get_now_status', function (req, res) {
    const { user_id } = req.body

    var sql = `
    select thj_account.thj_acc_status as status_id 
    FROM thj_account
    WHERE thj_account.thj_acc_id = ?`;

    db.query(sql, user_id, function (error, results, fields) {
        if (error) throw error;
        if (results) {
            var status_id = results[0]
            res.status(200).send(status_id);

        } else {
            res.status(200).send(false);
        }
    });
})
module.exports = router;
