<?php
    session_start();
    if (!isset($_SESSION['thj_acc_email_user'])) {
      header('location: index.php');
    }
    if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['thj_acc_email_user']);
      header('location: index.php');
    }
    require("src/conn.php");
    $usernames=$_SESSION['thj_acc_email_user'];
    mysqli_query($conn,"SET CHARACTER SET UTF8");
    $sql_user = "SELECT * FROM thj_account 
    INNER JOIN thj_account_role ON thj_account.thj_acc_status=thj_account_role.thj_account_role_id WHERE thj_acc_email_user ='$usernames'";
    $excq_user = $conn -> query($sql_user);
    $row_user = $excq_user -> fetch_assoc();
 

$_SESSION['id'] = $row_user['thj_acc_id'];

?>
<!DOCTYPE html>
<html lang="en"
  class="light-style layout-menu-fixed"
  dir="ltr"
  data-theme="theme-default"
  data-asset-path="asset/"
  data-template="vertical-menu-template-free">
  
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TCR Online</title>
    
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://kit.fontawesome.com/0949ce2d03.js" crossorigin="anonymous"></script>
    
  
     <!-- Favicon -->
     <link rel="icon" type="image/x-icon" sizes="16*16" href="assets/img/logo.png" />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="asset/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="asset/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="asset/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="asset/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="asset/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <link rel="stylesheet" href="asset/vendor/libs/apex-charts/apex-charts.css" />

    <!-- Page CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script> -->

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script> -->
    
    <!-- Helpers -->
    <script src="asset/vendor/js/helpers.js"></script>
    
    <script src="asset/js/config.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <script src="https://kit.fontawesome.com/0949ce2d03.js" crossorigin="anonymous"></script>
    

    
</head>
<body> 
<div class="layout-wrapper layout-content-navbar">
      <div class="layout-container">
    <!-- <div class="container"> -->
    <?php include("page/menu/menu_major.php"); ?>
        
        <!-- Layout container -->
        <div class="layout-page">
             <!-- Navbar -->

          <nav
            class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
            id="layout-navbar"
          >
            <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
              <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                <i class="bx bx-menu bx-sm"></i>
              </a>
            </div>

            <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
              <!-- Search -->
              <div class="navbar-nav align-items-center">
                <div class="nav-item d-flex align-items-center">
                  <i class="bx bx-search fs-4 lh-0"></i>
                  <input
                    type="text"
                    class="form-control border-0 shadow-none"
                    placeholder="Search..."
                    aria-label="Search..."
                  />
                </div>
              </div>
              <!-- /Search -->

              <ul class="navbar-nav flex-row align-items-center ms-auto">
                <!-- Place this tag where you want the button to render. -->
                <li class="nav-item lh-1 me-3">
                  <a><b><?php echo $row_user["thj_acc_name"]; ?></b></a>
                </li>

                <!-- User -->
                <li class="nav-item navbar-dropdown dropdown-user dropdown">
                  <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <div class="avatar avatar-online">
                      <img src="assets/img/admin.svg" alt class="w-px-40 h-auto rounded-circle" />
                    </div>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-end">
                    <li>
                      <a class="dropdown-item" href="#">
                        <div class="d-flex">
                          <div class="flex-shrink-0 me-3">
                            <div class="avatar avatar-online">
                              <img src="assets/img/admin.svg" alt class="w-px-40 h-auto rounded-circle" />
                            </div>
                          </div>
                          <div class="flex-grow-1">
                            <span class="fw-semibold d-block"><?php echo $row_user["thj_acc_name"]; ?></span>
                            <small class="text-muted"><?php echo $row_user["thj_account_role_name"]; ?></small>
                          </div>
                        </div>
                      </a>
                    </li>
                    <li>
                      <div class="dropdown-divider"></div>
                    </li>
                    <!-- <li>
                      <a class="dropdown-item" href="admin.php?page=profile">
                        <i class="bx bx-user me-2"></i>
                        <span class="align-middle">โปรไฟล์ของฉัน</span>
                      </a>
                    </li> -->
                    <li>
                      <div class="dropdown-divider"></div>
                    </li>
                    <li>
                      <a class="dropdown-item" href="admin.php?logout='1'">
                        <i class="bx bx-power-off me-2 text-danger"></i>
                        <span class="align-middle text-danger">ออกจากระบบ</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <!--/ User -->
              </ul>
            </div>
          </nav>

          <!-- / Navbar -->
           <!-- Content wrapper -->
           <div class="content-wrapper">
            <!-- Content -->
            <?php 
                if(isset($_GET["page"])){
                    $page = $_GET["page"];
                    include("page/$page/$page.php");
                }
                elseif(isset($_GET["pages"]) && isset($_GET["func"])){
                    $page = $_GET["pages"];
                    $func = $_GET["func"];
                    include("page/$page/$func.php");
                }
                else{
                    include("page/home/home.php");
                }
                ?>
          </div>
          <!-- Content wrapper -->
        </div>
        </div>
        </div>
    </div>

    <!-- build:js asset/vendor/js/core.js -->
    <script src="asset/vendor/libs/jquery/jquery.js"></script>
    <script src="asset/vendor/libs/popper/popper.js"></script>
    <script src="asset/vendor/js/bootstrap.js"></script>
    <script src="asset/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

    <script src="asset/vendor/js/menu.js"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="asset/vendor/libs/apex-charts/apexcharts.js"></script>

    <!-- Main JS -->
    <script src="asset/js/main.js"></script>

    <!-- Page JS -->
    <script src="asset/js/dashboards-analytics.js"></script>

</body>
</html>