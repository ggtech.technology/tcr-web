<?php
    $sql_ma="SELECT * FROM thj_major where thj_major_user_id=".$row_user["thj_acc_id"];
    $qry_ma = $conn -> query($sql_ma);
    $maa = $qry_ma -> fetch_assoc();

    $query=mysqli_query($conn,"SELECT COUNT(thj_acc_id) FROM thj_account LEFT JOIN thj_account_role ON thj_account.thj_acc_status = thj_account_role.thj_account_role_id
    WHERE thj_account_role.thj_account_role_id = 3 AND thj_account.thj_acc_id_point !=0");
    $row = mysqli_fetch_row($query);

    $nquery2=mysqli_query($conn,"SELECT
    thj_order.thj_order_id as order_id,
    thj_order.thj_order_req_id as rec_id,
    thj_account_role.thj_account_role_name as acc_role,
    thj_account.thj_acc_name as acc_name,
    thj_account.thj_acc_id as acc_id,
    thj_order.thj_order_date as order_date,
    thj_order.thj_order_time as order_time,
    thj_acc_seq_id
    FROM thj_order
    LEFT JOIN thj_account ON thj_order.thj_order_acc_byP = thj_account.thj_acc_id
    LEFT JOIN thj_account_role ON thj_account.thj_acc_status = thj_account_role.thj_account_role_id
    WHERE thj_account.thj_acc_status = 5
    GROUP BY thj_account.thj_acc_name");
        
    
?>
                <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก / ประวัติการการซื้อ /</span> สมาชิก</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
    <div class="card-header d-flex justify-content-between">
        <h4>ประวัติการการซื้อ(สมาชิก)
</h4>
        <!-- <a href="./admin.php?pages=product&func=insert_product" class="btn btn-dark"><i class="fa-solid fa-plus"></i> เพิ่มสมาชิก</a> -->
    </div>

  <div class="table-responsive text-nowrap">
    
    
</div>
<!--/ Basic Bootstrap Table -->
        </div>
        <div class="row">
          
                <div class="col-md mb-4 mb-md-0">
                  <div class="accordion mt-3" id="accordionExample">
                  <?php while($row = $nquery2->fetch_assoc()){ ?>
                    <div class="card accordion-item">
                      <h2 class="accordion-header" id="headingOne">
                        <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordion<?php echo $row['acc_id'] ?>" aria-expanded="false" aria-controls="accordion<?php echo $row['acc_id'] ?>">
                        <strong><?php echo "[".$row['thj_acc_seq_id']."]  "?><?php echo $row['acc_name']?></strong>
                        </button>
                      </h2>

                      <div id="accordion<?php echo $row['acc_id'] ?>" class="accordion-collapse collapse" data-bs-parent="#accordionExample" style="">
                        <div class="accordion-body">
                          <table class="table table-hover">
                            <thead>
                              <tr>
                                <!-- <th>ชื่อผู้ใช้งาน</th> -->
                                <th>รหัสออเดอร์</th>
                                <th>วันที่</th>
                                <th>ช่องทางการส่งของ</th>
                                <th>รายละเอียด</th>
                              </tr>
                            </thead>
                            <tbody class="table-border-bottom-0">
                              <?php $sqll="SELECT thj_order.thj_order_id as order_id,
                                              thj_order.thj_order_req_id as rec_id,
                                              thj_order.thj_order_prod_list as order_list ,
                                              thj_order.thj_order_prod_amount as order_amount ,
                                              thj_order.thj_order_all_price as order_price,
                                              thj_order.thj_order_date as order_date ,
                                              thj_order.thj_order_time as order_time ,
                                              thj_account.thj_acc_name as acc_name ,
                                              thj_order_type.thj_order_type_name as payment_method,
                                              thj_order_status.thj_order_status_name as order_status,
                                              thj_payment_order.thj_payment_proof as payment_proof,
                                              thj_acc_address_all.thj_acc_address_detail as addr_detail,
                                              provinces.name_th as provinces_th,
                                              amphures.name_th as amphures_th,thj_order_delivery_fee,
                                              districts.name_th as districts_th
                                              FROM thj_order
                                              LEFT JOIN thj_account ON thj_order.thj_order_acc_byP = thj_account.thj_acc_id
                                              LEFT JOIN thj_major ON thj_order.thj_order_major_id = thj_major.thj_major_id
                                              LEFT JOIN thj_order_status ON thj_order.thj_order_status = thj_order_status.thj_order_status_id
                                              LEFT JOIN thj_account_role ON thj_account.thj_acc_status = thj_account_role.thj_account_role_id
                                              LEFT JOIN thj_order_type ON thj_order.thj_order_type = thj_order_type.thj_order_type_id
                                              LEFT JOIN thj_acc_address_all ON thj_order.thj_order_prod_sendTo = thj_acc_address_all.thj_acc_address_id
                                              left JOIN provinces ON thj_acc_address_all.thj_acc_address_provinces = provinces.id
                                              left JOIN amphures ON thj_acc_address_all.thj_acc_address_amphures = amphures.id
                                              left JOIN districts ON thj_acc_address_all.thj_acc_address_districts = districts.id
                                              LEFT JOIN thj_payment_order ON thj_order.thj_order_id = thj_payment_order.thj_payment_order_id
                                              where thj_account.thj_acc_id=".$row['acc_id']."
                                                   ORDER BY thj_order.thj_order_id DESC";
                                  $orr=mysqli_query($conn,$sqll);
                                  while($rows = $orr->fetch_assoc()){
                              ?>
                              <tr>
                                <!-- <td></td> -->
                                <td><strong><?php echo $rows['rec_id'] ?></strong></td>
                                <td><?php echo $rows["order_date"]." ".$rows["order_time"] ?></td>
                                <td><?php echo $rows['payment_method'] ?></td>
                                <td><div class="row">
                                  <div class="col-sm-12">
                                  <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th>รายการสินค้า</th>
                                          <th>จำนวน</th>
                                          <th>ราคา</th>
                                          <th>รวม</th>
                                        </tr>
                                      </thead>
                                      <tbody class="table-border-bottom-0">
                                        <?php 
                                                $order_list=explode(', ',$rows["order_list"]);
                                                $order_amount=explode(',',$rows["order_amount"]);
                                                $order_price=explode(',',$rows["order_price"]);
                                                $sum=0;
                                                $sum1=0;
                                                for($i=0; $i < count($order_list); $i++){
                                                    $sum=$order_amount[$i]*$order_price[$i];
                                                    $sum1+=$sum;
                                                    echo "<tr>
                                                            <td><b>".$order_list[$i]."</b></td>
                                                            <td>".$order_amount[$i]."</td>
                                                            <td>".$order_price[$i]."</td>
                                                            <td>".$sum."</td>
                                                        </tr>";
                                                } 
                                              
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>ค่าส่ง</th>
                                            <td><b><?php echo $rows["thj_order_delivery_fee"] ?></b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>ราคารวม</th>
                                            <td><b><?php echo $sum1+$rows["thj_order_delivery_fee"] ?></b></td>
                                        </tr>
                                        
                                      </tbody>
                                    </table>
                                  </div>
                                  </div>
                                  </td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                
              </div>
        <?php // include('detail_point.php'); ?>

                        
        