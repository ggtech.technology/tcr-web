    <div class="mt-3">
        <?php while($rows = $nquery1->fetch_assoc()){ ?>
        <!-- Modal -->
        <div class="modal fade" id="modalCenter<?php echo $rows["thj_acc_id"] ?>" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-l" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $rows['thj_acc_name']?></b></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
                </div>
                <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                    <div class="card table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>โทรศัพท์</th>
                            <td><?php 
                                    if($rows['thj_acc_tel']!=''){
                                    $tel=str_split($rows['thj_acc_tel']);
                                    echo $tel[0].$tel[1].$tel[2].'-'.$tel[3].$tel[4].$tel[5].' '.$tel[6].$tel[7].$tel[8].$tel[9];
                                    } 
                                ?>
                    </td>
                        </tr>
                        <tr>
                            <th>หมายเลขบัตรประชาชน</th>
                            <!-- <td><img class="img-thumbnail img-fluid rounded mx-auto d-block" src="<?php echo $rows["thj_acc_id_card"] ?>" alt="" ></td> -->
                            <td><?php echo $rows["thj_acc_number_card"] ?></td>
                        </tr>
                        <tr>
                            <th>วันที่ลงทะเบียน</th>
                            <td><?php
                                        $date = new DateTime($rows['thj_acc_dateT_regis']);
                                        $dm = $date->format('d-m-');
                                        $y = $date->format('Y')+543;
                                        echo $dm.$y;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>คำเชิญ</th>
                            <td><?php echo $rows["thj_acc_code_saler"] ?></td>
                        </tr>
                        <tr>
                            <th>สถานะ</th>
                            <td>
                            <?php //if($rows["thj_acc_status"] == '1'){ ?>
                                <span class="badge bg-label-primary me-1">บุคคลทั่วไป</span>
                            <?php //} ?>
                            </td>
                        </tr>
                        </thead>
                    </table>
                    </div>
                    </div></div>
                    </div>
                </div>
                </div>
            </div>
            <?php } ?>
        </div>