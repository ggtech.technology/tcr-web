<div class="mt-3">
                      <?php while($rows = $nquery1->fetch_assoc()){ ?>
                        <!-- Modal -->
                        <div class="modal fade" id="modalCenter1<?php echo $rows["order_id"] ?>" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $rows['rec_id']?></b></h5>
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-sm-6">
                                  <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th>รายการสินค้า</th>
                                          <th>จำนวน</th>
                                          <th>ราคา</th>
                                          <th>รวม</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php 
                                                $order_list=explode(', ',$rows["order_list"]);
                                                $order_amount=explode(',',$rows["order_amount"]);
                                                $order_price=explode(',',$rows["order_price"]);
                                                $sum=0;
                                                $sum1=0;
                                                for($i=0; $i < count($order_list); $i++){
                                                    $sum=$order_amount[$i]*$order_price[$i];
                                                    $sum1+=$sum;
                                                    echo "<tr>
                                                            <td><b>".$order_list[$i]."</b></td>
                                                            <td>".$order_amount[$i]."</td>
                                                            <td>".$order_price[$i]."</td>
                                                            <td>".$sum."</td>
                                                        </tr>";
                                                }
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>ค่าส่ง</th>
                                            <td><b><?php echo (int)$rows["thj_order_delivery_fee"] ?></b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>ราคารวม</th>
                                            <td><b><?php echo $sum1+(int)$rows["thj_order_delivery_fee"] ?></b></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="col-sm-6">
                                  <div class="card table-responsive">
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                        <th>วันที่สั่งซื้อ</th>
                                          <td><?php 
                                                    $date = new DateTime($rows['order_date']); 
                                                    $dm = $date->format('d-m-');
                                                    $y = $date->format('Y')+543;

                                                    $time = new DateTime($rows['order_time']);
                                                    $time_ = $time->format('H:i');
                                                    echo $dm.$y.'&nbsp;&nbsp;&nbsp;&nbsp;'.$time_;
                                          ?> </td>
                                        </tr>
                                        <tr>
                                          <th>ชื่อลูกค้า</th>
                                          <td><?php echo $rows["acc_name"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ช่องทางการชำระเงิน</th>
                                          <td><?php echo $rows["payment_method"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ที่อยู่</th>
                                          <td><?php echo $rows['districts_th'].' '.$rows['amphures_th'].' '.$rows['provinces_th'].' '.$rows['addr_detail']?></td>
                                        </tr>
                                        <tr>
                                          <th>สถานะ</th>
                                          <td>
                                            <?php if($rows['order_status'] == 'รอชำระเงิน'){
                                                    echo '<span class="badge bg-label-primary me-1">รอชำระเงิน</span>';
                                                    }
                                                    elseif($rows['order_status'] == 'รอตรวจสอบ'){
                                                    echo '<span class="badge bg-label-dark me-1">รอตรวจสอบ</span>';
                                                    }
                                                    elseif($rows['order_status'] == 'เตรียมจัดส่ง'){
                                                    echo '<span class="badge bg-label-info me-1">เตรียมจัดส่ง</span>';
                                                    }
                                                    elseif($rows['order_status'] == 'กำลังจัดส่ง'){
                                                        echo '<span class="badge bg-label-warning me-1">กำลังจัดส่ง</span>';
                                                    }
                                                    elseif($rows['order_status'] == 'รับสินคัาแล้ว'){
                                                        echo '<span class="badge bg-label-success me-1">นำส่งสำเร็จ</span>';
                                                    }
                                                    elseif($rows['order_status'] == 'ไม่สำเร็จ'){
                                                        echo '<span class="badge bg-label-danger me-1">ลูกค้าปฏิเสธการรับสินค้า</span>';
                                                      }
                                                    elseif($rows['order_status'] == 'ยกเลิกการสั่งซื้อ'){
                                                      echo '<span class="badge bg-label-danger me-1">ยกเลิกการสั่งซื้อ</span>';
                                                    }
                                            ?>
                                        </td>
                                        </tr>
                                      </thead>
                                    </table>
                                    </div>
                                    </div></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <?php } ?>
                        </div>