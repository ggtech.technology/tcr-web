<div class="modal fade" id="promo" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
            <h5 class="modal-title" id="modalCenterTitle"><b>โปรโมชั่นสินค้า</b></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
            </div>
            <div class="modal-body">
            <div class="card-body">
                <form action="http://203.150.243.105/serve/etc/add_promotion_type" method="POST" >
                <div class="row">
                    <div class="mb-3 col-md-5">
                    <label for="address" class="form-label">ประเภท</label>
                    <select class="select2 form-select" name="type_id" id="type_id" required>
                                <option selected disabled>--กรุณาเลือกชนิดสินค้า--</option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['thj_type_prod_id']?>" ><?php echo $row_tp['thj_type_prod_name']?></option>
                                <?php } ?>
                            </select>
                    </div>
                    <div class="mb-3 col-md-5">
                    <label for="state" class="form-label">จำนวนสินค้าที่ต้องซื้อ</label>
                    <input class="form-control" type="number" id="state" name="amount" placeholder="000">
                    </div>
                    <div class="mb-3 col-md-2">
                    <label for="zipCode" class="form-label">เปอร์เซ็นต์ที่ลด</label>
                    <input type="number" class="form-control" id="zipCode" name="discount" placeholder="000">
                    </div>
                    <div class="col-md-8">
                    <div class="d-flex mb-2">
                            <div class="input-group ">
                              <div class="input-group-prepend">
                                  <div class="input-group-text">วันที่(จาก)</div>
                              </div>
                              <input type="date" name="thj_promotion_type_start" id="" class="form-control ">
                            </div>&nbsp;&nbsp;&nbsp;
                            <div class="input-group ">
                              <div class="input-group-prepend">
                                  <div class="input-group-text">วันที่(ถึง)</div>
                              </div>
                              <input type="date" name="thj_promotion_type_end" id="" class="form-control " value="<?php echo date("Y-m-d") ?>">
                            </div>
                    </div> 
                </div>
                </div>
                <div class="mt-2">
                    <button type="submit" class="btn btn-primary me-2">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
