<?php

    $sql_ma="SELECT * FROM thj_major where thj_major_user_id=".$row_user["thj_acc_id"];
    $qry_ma = $conn -> query($sql_ma);
    $maa = $qry_ma -> fetch_assoc();

    $query=mysqli_query($conn,"SELECT COUNT(thj_acc_id) FROM thj_account LEFT JOIN thj_account_role ON thj_account.thj_acc_status = thj_account_role.thj_account_role_id
    WHERE thj_account_role.thj_account_role_id = 3 AND thj_account.thj_acc_id_point !=0");
    $row = mysqli_fetch_row($query);

    $nquery2=mysqli_query($conn,"SELECT thj_promotion_type_id,thj_type_product.thj_type_prod_name as type_name,
    thj_promotion_type.thj_promotion_type_amount as amount,
    thj_promotion_type.thj_promotion_type_discount as discount,thj_promotion_type_start,thj_promotion_type_end
    FROM thj_promotion_type
    LEFT JOIN thj_type_product ON thj_promotion_type.thj_promotion_type_type_id = thj_type_product.thj_type_prod_id");

    function httpPost1($url)
    {
        $curl_int = curl_init(); 
        curl_setopt($curl_int,CURLOPT_URL,$url);
        curl_setopt($curl_int,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl_int,CURLOPT_HEADER, false); 
        $output=curl_exec($curl_int);
        curl_close($curl_int);

        $obj = json_decode($output);
        // $type_id = $obj->type_id;
        // $amount = $obj->amount;
        // $discount = $obj->discount;
        $myJSON = json_encode($obj);
        
        return $myJSON;
        }

    // $d1=httpPost1('http://203.150.243.105/serve/etc/select_promotion_type');
    // echo $d1;
    $sql_tp="SELECT * FROM thj_type_product where thj_type_prod_status != 9";
        $qry_tp=mysqli_query($conn,$sql_tp);
?>
                <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก</span> / โปรโมชั่น</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
    <div class="card-header d-flex justify-content-between">
        <h4></h4>
        <a href="" data-bs-toggle="modal" data-bs-target="#promo" class="btn btn-dark"><i class="fa-solid fa-plus"></i> เพิ่มโปรโมชั่น</a> 
    </div>

  <div class="table-responsive text-nowrap">
  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>ประเภทสินค้า</th>
                        <th>จำนวนสินค้า</th>
                        <th>ส่วนลดการส่ง</th>
                        <th>เริ่มต้น</th>
                        <th>สิ้นสุด</th>
                        <th>ลบ</th>
                      </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                        <?php $sum2=0; while($row = $nquery2->fetch_assoc()){ ?>
                                <td><b><?php echo $row['type_name']?></b></td>
                                <td><?php echo $row['amount']." ชิ้น" ?></td>
                                <td><?php echo $row['discount']." %" ?></td>
                                <td><?php
                                        $date = new DateTime($row['thj_promotion_type_start']);
                                        $dm = $date->format('d-m-Y');
                                        echo $dm;
                                        // echo $row['thj_promotion_type_start'] 
                                        ?></td>
                                <td><?php
                                        $dates = new DateTime($row['thj_promotion_type_end']);
                                        $dms = $dates->format('d-m-Y');
                                        echo $dms;
                                        // echo $row['thj_promotion_type_end'] 
                                        ?></td>
                                <td><a href="" data-bs-toggle="modal" data-bs-target="#delete<?php echo $row["thj_promotion_type_id"] ?>"><i class="fa-solid fa-trash text-danger"></i></a></td>
                            </tr>
                            <div class="modal fade" id="delete<?php echo $row['thj_promotion_type_id'] ?>" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="modalCenterTitle"><b>ยืนยันการลบ</b></h5>
                                    <button
                                        type="button"
                                        class="btn-close"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                    ></button>
                                    </div>
                                    <div class="modal-body" align="center">
                                        <div class="row" >
                                            <i class="fa-solid fa-circle-xmark text-danger" style="font-size: 5rem;"></i>
                                            <h4 class="mt-3"> คุณต้องการลบใช่หรือไม่ ?</h4>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    <form action="page/promo/de_promo.php" method="post">
                                        <input type="text" name="thj_promotion_type_id" id="" value="<?php echo $row["thj_promotion_type_id"] ?>" hidden>
                                        <button type="submit" class="btn btn-outline-danger">ลบ</button>
                                    </form>
                                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal"> ยกเลิก </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php } ?>
                    </tbody>
                  </table>
</div>
<!--/ Basic Bootstrap Table -->
        </div>
        <?php  include('insert_promo.php'); ?>

                        
        