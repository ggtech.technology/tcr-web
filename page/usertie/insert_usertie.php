<div class="modal fade" id="seller" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
            <h5 class="modal-title" id="modalCenterTitle"><b>คำขอสมัครเซลล์แมน</b></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive text-nowrap">
                    <table class="table table-striped">
                    <caption class="ms-4">
                                <div id="pagination_controls" style="display: flex;">
                                    <div class="pagination">
                                        <?php echo $paginationCtrls; ?>
                                    </div>
                </div>
                                    </caption>
                    <thead>
                        <tr>
                            <th>ชื่อ-สกุล</th>
                            <th>ชื่อผู้ใช้งาน</th>
                            <th>จำนวนแต้ม</th>
                            <th>วันและเวลาสมัคร</th>
                            <th>ใบอนุญาต</th>
                            <th>หลักฐานการโอน</th>
                            <th>สถานะ</th>
                            <!-- <th>ดูรายละเอียด</th> -->
                        </tr>
                    </thead>
                    <?php 
                            require("src/conn.php");
                            
                            mysqli_query($conn,"SET CHARACTER SET UTF8"); 
                            while($rows = $nquery2->fetch_assoc()){
                        ?> 
                    <tbody class="table-border-bottom-0">
                        <tr>
                            <td><?php echo $rows['thj_acc_name']?></td>
                            <td><?php echo $rows['thj_acc_email_user']?></td>
                            <td><?php echo $rows['thj_acc_id_point']?></td>
                            <td><?php echo $rows['thj_seller_request_date']?></td>
                            <td><a href="" data-bs-toggle="modal" data-bs-target="#img2<?php echo $rows["thj_acc_id"] ?>"><i class="fa-solid fa-address-card text-primary"></i></a></td>
                            <td><a href="" data-bs-toggle="modal" data-bs-target="#img1<?php echo $rows["thj_acc_id"] ?>"><i class="fa-solid fa-file-invoice-dollar text-danger"></i></a></td>
                            
                            <td><span class="badge bg-label-success me-1">เซลล์แมน</span></td>
                            <!-- <td><a href="" data-bs-toggle="modal" data-bs-target="#modalCenter<?php echo $rows["thj_acc_id"] ?>"><i class="fa-solid fa-eye text-success"></i></a></td> -->
                            
                        <!-- <td><a href="./admin.php?pages=user&func=edit_user&user_id=<?php echo $rows["thj_acc_id"]?>"><i class="fa-solid fa-pen-to-square text-primary"></i></a></td> -->
                        <td><a href="" data-bs-toggle="modal" data-bs-target="#approve1<?php echo $rows["thj_acc_id"]?>"><button class="btn btn-dark" >อนุมัติ</button></a></td>
                        </tr>
                    </tbody>
                    
                       
                    <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php   mysqli_query($conn,"SET CHARACTER SET UTF8"); 
        while($rowb = $nquery3->fetch_assoc()){ ?> 
        <div class="modal fade" id="approve1<?php echo $rowb['thj_acc_id'] ?>" tabindex="-1" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="modalCenterTitle"><b>ยืนยันการอนุมัติการเป็นเซลล์</b></h5>
                                <button
                                    type="button"
                                    class="btn-close"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                ></button>
                                </div>
                                <div class="modal-body" align="center">
                                    <div class="row" >
                                        <i class="fa-solid fa-clipboard-check text-success" style="font-size: 5rem;"></i>
                                        <h4 class="mt-3"> คุณต้องการอนุญาต <b><?php echo $rowb['thj_acc_name']?></b> ให้เป็นเซลล์ใช่หรือไม่ ?</h4>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                <form action="http://203.150.243.105/serve/Account/seller_approve" method="post">
                                    <input type="text" name="req_id" id="" value="<?php echo $rowb["thj_seller_request_id"] ?>" hidden>
                                    <input type="text" name="acc_id" id="" value="<?php echo $rowb["thj_acc_id"] ?>" hidden>
                                    <button type="submit" class="btn btn-outline-success">อนุมัติ</button>
                                </form>
                                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal"> ยกเลิก </button>
                                </div>
                            </div>
                            </div>
                        </div>
        <div class="modal fade" id="img1<?php echo $rowb['thj_acc_id'] ?>" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                            <div class="modal-content">
                              <div class="modal-header"> 
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                  <div class="row">
                                      <img class="img-thumbnail img-fluid rounded mx-auto d-block" src="<?php echo $rowb['thj_seller_request_proof']?>" alt="" >
                                  </div>
                              </div>
                            </div>
                          </div>
        </div>
        <div class="modal fade" id="img2<?php echo $rowb['thj_acc_id'] ?>" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                            <div class="modal-content">
                              <div class="modal-header"> 
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                  <div class="row">
                                      <img class="img-thumbnail img-fluid rounded mx-auto d-block" src="<?php echo $rowb['thj_acc_id_card']?>" alt="" >
                                  </div>
                              </div>
                            </div>
                          </div>
        </div>
<?php } ?>