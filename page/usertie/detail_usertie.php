    <div class="mt-3">
        <?php while($rows = $nquery1->fetch_assoc()){ ?>
        <!-- Modal -->
        <div class="modal fade" id="modalCenter<?php echo $rows["thj_acc_id"] ?>" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-l" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $rows['thj_acc_name']?></b></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
                </div>
                <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                    <div class="card table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>วัน-เวลาสมัคร</th>
                            <td><?php echo $rows["thj_acc_dateT_regis"] ?></td>
                        </tr>
                        <tr>
                            <th>ชื่อ-สกุล</th>
                            <td><?php echo $rows["thj_acc_name"] ?></td>
                        </tr>
                        <tr>
                            <th>อีเมล์</th>
                            <td><?php echo $rows["thj_acc_email_user"] ?></td>
                        </tr>
                        <tr>
                            <th>หมายเลขบัตรประชาชน</th>
                            <!-- <td><img class="img-thumbnail img-fluid rounded mx-auto d-block" src="<?php echo $rows["thj_acc_id_card"] ?>" alt="" ></td> -->
                            <td><?php echo $rows["thj_acc_number_card"] ?></td>
                        </tr>
                        <tr>
                            <th>ชื่อธนาคาร</th>
                            <td><?php echo $rows["thj_acc_banking_name"] ?></td>
                        </tr>
                        <tr>
                            <th>เลขบัญชี</th>
                            <td><?php echo $rows["thj_acc_banking_number"]?></td>
                        </tr>
                        <tr>
                            <th>รหัสคำเชิญ</th>
                            <td><?php echo $rows["thj_acc_code_saler"] ?></td>
                        </tr>
                        <tr>
                            <th>สถานะ</th>
                            <td>
                            <?php //if($rows["thj_acc_status"] == '1'){ ?>
                                <span class="badge bg-label-success me-1">เซลล์แมน</span>
                            <?php //} ?>
                            </td>
                        </tr>
                        </thead>
                    </table>
                    </div>
                    </div></div>
                    </div>
                </div>
                </div>
            </div>
            <?php } ?>
        </div>