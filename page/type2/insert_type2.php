<div class="mt-3">

        <!-- Modal -->
        <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-l" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle"><b>เพิ่มประเภทที่ 2</b></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
                </div>
                <div class="modal-body">
                <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
                <!-- <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> <span class="text-muted fw-light">ข้อมูลสาขา /</span> แก้ไขสาขา</h4> -->

              <div class="row">                                               
                <div class="col-md-12"> 
                  <div class="card mb-2">
                    <h4 class="card-header">เพิ่มประเภทที่ 2</h4>
                    <!-- Account -->
                    <hr class="my-0" />
                    <div class="card-body">
                      <form action="http://203.150.243.105/serve/Product/add_type2" method="post" >
                        <div class="row">
                          <div class="mb-3">
                            <label for="firstName" class="form-label">ประเภทที่ 1</label>
                            <select class="select2 form-select" name="t1_id" id="t1_id" required>
                                <option selected disabled>--ประเภทที่ 1--</option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['thj_type_prod_id']?>" ><?php echo $row_tp['thj_type_prod_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3">
                            <label for="firstName" class="form-label">ประเภทที่ 2</label>
                            <input
                              class="form-control"
                              type="text"
                              id="firstName"
                              name="t2_name"
                              placeholder="ประเภทที่ 2"
                              value=""
                              required
                            />
                          </div>
                        </div>
                        <div class="mt-2" align="center">
                          <button type="submit" name="submit" class="btn btn-primary me-2">บันทึก</button>
                          <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                        </div>
                      </form>
                    </div>
                    <!-- /Account -->
                  </div>
                </div>
              </div>
            </div>
          </div>
                </div>
                </div>
                </div>
            </div>
        
        </div>