  <script type="text/javascript">
      $(document).ready(function(){
        $("#provinces").change(function(){
          var provinces = $(this).val();
          $.ajax({
            url: 'page/major/data/data.php',
            method: 'post',
            data: {id:provinces,function:'provinces'},
            success: function(data){
              console.log(data);
              $('#amphures').html(data);
              // console.log(data);
            }
          })
          })
          $("#amphures").change(function(){
          var amphures = $(this).val();
          $.ajax({
            url: 'page/major/data/data.php',
            method: 'post',
            data: {id:amphures,function:'amphures'},
            success: function(data){
              console.log(data);
              $('#districts').html(data);
            }
          })
          })
        })
    </script>
    <?php 
        require("src/conn.php");
        mysqli_query($conn,"SET CHARACTER SET UTF8"); 
        $sql_ma="SELECT * FROM thj_account where thj_acc_status=4";
        $qry_ma = $conn -> query($sql_ma);
        // $maa = $qry_ma -> fetch_assoc();

        $sql_tp="SELECT * FROM provinces";
        $qry_tp=mysqli_query($conn,$sql_tp);

        $sql_am="SELECT * FROM amphures";
        $qry_am=mysqli_query($conn,$sql_am);

        $sql_dis="SELECT * FROM districts";
        $qry_dis=mysqli_query($conn,$sql_dis);
    ?>
       <!-- Content wrapper -->
       <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> <span class="text-muted fw-light">ข้อมูลสาขา /</span> เพิ่มสาขา</h4>

              <div class="row">
                <div class="col-md-12">
                  <div class="card mb-2">
                    <h4 class="card-header">เพิ่มสาขา</h4>
                    <!-- Account -->
                    <hr class="my-0" />
                    <div class="card-body">
                      <form action="http://203.150.243.105/serve/Major/register_major" method="post" >
                        <div class="row">
                          <div class="mb-3 col-md-6">
                            <label for="firstName" class="form-label">ชื่อสาขา</label>
                            <input
                              class="form-control"
                              type="text"
                              id="firstName"
                              name="major_name"
                              placeholder="ชื่อสาขา"
                              autofocus
                              required
                            />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="zipCode" class="form-label">จังหวัด</label>
                            <select class="select2 form-select" name="major_province_id" id="provinces"  required>
                                <option selected disabled>--กรุณาเลือกจังหวัด--</option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['id']?>" ><?php echo $row_tp['name_th']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">อำเภอ</label>
                            <select class="select2 form-select" name="major_amphures_id" id="amphures" >
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ตำบล</label>
                            <select class="select2 form-select" name="major_district_id" id="districts" >
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ผู้ดูแลสาขา</label>
                            <select class="select2 form-select" name="major_user_id" >
                            <option selected disabled>--กรุณาเลือกผู้ดูแลสาขา--</option>
                            <?php while($row_ma=mysqli_fetch_array($qry_ma)){ ?>
                                    <option value="<?php echo $row_ma['thj_acc_id']?>" ><?php echo $row_ma['thj_acc_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">เบอร์โทร</label>
                            <input class="form-control" type="text" name="major_tel" placeholder="เบอร์โทร" required id="lastName" />
                          </div>
                        </div>
                        <div class="mt-2">
                          <button type="submit" name="submit" class="btn btn-primary me-2">บันทึก</button>
                          <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                          <button onclick="history.back()" class="btn btn-outline-secondary">กลับ</button>
                        </div>
                      </form>
                    </div>
                    <!-- /Account -->
                  </div>
                </div>
              </div>
            </div>
          </div>
            <!-- / Content -->
            