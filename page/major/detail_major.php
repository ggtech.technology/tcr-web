<div class="mt-3">
        <?php while($rows = $nquery1->fetch_assoc()){ ?>
        <!-- Modal -->
        <div class="modal fade" id="modalCenter<?php echo $rows["thj_major_id"] ?>" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-l" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $rows['thj_major_name']?></b></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
                </div>
                <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                    <div class="card table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ชื่อผู้จัดการ</th>
                            <td><?php echo $rows["thj_acc_name"] ?></td>
                        </tr>
                        <tr>
                            <th>ที่อยู่</th>
                            <td><?php echo " ตำบล".$rows["disname"]." อำเภอ".$rows["amname"]." จังหวัด".$rows["proname"]?></td>
                        </tr>
                        <tr>
                            <th>โทรศัพท์</th>
                            <td><?php echo $rows["thj_major_tel"] ?></td>
                        </tr>
                        <tr>
                            <th>สถานะ</th>
                            <td>
                            <?php //if($rows["thj_acc_status"] == '1'){ ?>
                                <span class="badge bg-label-primary me-1">เปิดใช้งาน</span>
                            <?php //} ?>
                            </td>
                        </tr>
                        </thead>
                    </table>
                    </div>
                    </div></div>
                    </div>
                </div>
                </div>
            </div>
            <?php } ?>
        </div>