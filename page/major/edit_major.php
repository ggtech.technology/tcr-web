<script type="text/javascript">
      $(document).ready(function(){
        $("#provinces").change(function(){
          var provinces = $(this).val();
          $.ajax({
            url: 'page/major/data/data.php',
            method: 'post',
            data: {id:provinces,function:'provinces'},
            success: function(data){
              console.log(data);
              $('#amphures').html(data);
              // console.log(data);
            }
          })
          })
          $("#amphures").change(function(){
          var amphures = $(this).val();
          $.ajax({
            url: 'page/major/data/data.php',
            method: 'post',
            data: {id:amphures,function:'amphures'},
            success: function(data){
              console.log(data);
              $('#districts').html(data);
            }
          })
          })
        })
    </script>
<?php 
        require("src/conn.php");

        $major_id=$_GET['major_id'];

        mysqli_query($conn,"SET CHARACTER SET UTF8"); 
        $sql_ma="SELECT provinces.name_th as proname,provinces.id as pre_id,amphures.name_th as amname,amphures.id as am_id,districts.name_th as disname,districts.id as dis_id,thj_major_id,thj_major_name,thj_acc_name,thj_major_tel,thj_acc_id
        FROM thj_major
        LEFT JOIN provinces ON thj_major.thj_major_province_id = provinces.id
        LEFT JOIN amphures ON thj_major.thj_major_amphures_id = amphures.id
        LEFT JOIN districts ON thj_major.thj_major_district_id = districts.id
        LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id where thj_major_id=".$major_id;
        $qry_ma = $conn -> query($sql_ma);
        $maa = $qry_ma -> fetch_assoc();

        $sql_ma1="SELECT * FROM thj_account where thj_acc_status=4 or thj_acc_status=2";
        $qry_ma1 = $conn -> query($sql_ma1);
        // $maa1 = $qry_ma1 -> fetch_assoc();

        $sql_tp="SELECT * FROM provinces";
        $qry_tp=mysqli_query($conn,$sql_tp);

        $sql_am="SELECT * FROM amphures";
        $qry_am=mysqli_query($conn,$sql_am);

        $sql_dis="SELECT * FROM districts";
        $qry_dis=mysqli_query($conn,$sql_dis);
    ?>
       <!-- Content wrapper -->
       <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> <span class="text-muted fw-light">ข้อมูลสาขา /</span> แก้ไขสาขา</h4>

              <div class="row">
                <div class="col-md-12">
                  <div class="card mb-2">
                    <!-- <h4 class="card-header">สาขา</h4> -->
                    <!-- Account -->
                    <hr class="my-0" />
                    <div class="card-body">
                      <form action="http://203.150.243.105/serve/Major/edit_major_data" method="post" >
                        <input class="form-control" name="major_id" type="text" value="<?php echo $maa['thj_major_id']?>" hidden>
                        <div class="row">
                          <div class="mb-3 col-md-6">
                            <label for="firstName" class="form-label">ชื่อสาขา</label>
                            <input
                              class="form-control"
                              type="text"
                              id="firstName"
                              name="major_name"
                              value="<?php echo $maa['thj_major_name']?>"
                              placeholder="ชื่อสาขา"
                              autofocus
                              required
                            />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="zipCode" class="form-label">จังหวัด</label>
                            <select class="select2 form-select" name="major_province" id="provinces"  required>
                                <!-- <option selected disabled>--กรุณาเลือกจังหวัด--</option> -->
                                <option value="<?php echo $maa['pre_id']?>"><?php echo $maa['proname']?></option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    
                                    <option value="<?php echo $row_tp['id']?>" ><?php echo $row_tp['name_th']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">อำเภอ</label>
                            <select class="select2 form-select" name="major_amphures" id="amphures" >
                                <option value="<?php echo $maa['am_id']?>"><?php echo $maa['amname']?></option>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ตำบล</label>
                            <select class="select2 form-select" name="major_district" id="districts" >
                                <option value="<?php echo $maa['dis_id']?>"><?php echo $maa['disname']?></option>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ผู้ดูแลสาขา</label>
                                <input class="form-control" name="owner_id" type="text" value="<?php echo $maa['thj_acc_id']?>" hidden>
                                <input class="form-control" type="text" value="<?php echo $maa['thj_acc_name']?>" disabled>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">เบอร์โทร</label>
                            <input class="form-control" type="text" name="major_tel" value="<?php echo $maa['thj_major_tel']?>" placeholder="เบอร์โทร" required id="" />
                          </div>
                        </div>
                        <div class="mt-2">
                          <button type="submit" name="submit" class="btn btn-primary me-2">บันทึก</button>
                          <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                          <button onclick="history.back()" class="btn btn-outline-secondary">กลับ</button>
                        </div>
                      </form>
                    </div>
                    <!-- /Account -->
                  </div>
                </div>
              </div>
            </div>
          </div>
            