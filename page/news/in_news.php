<div class="modal fade" id="promo" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
            <h5 class="modal-title" id="modalCenterTitle"><b>เกล็ดความรู้</b></h5>
                <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
            </div>
            <div class="modal-body">
            <div class="card-body">
                <form action="http://203.150.243.105/serve/etc/add_tips" method="POST" >
                <div class="row">
                    <!-- <div class="mb-3 col-md-5">
                    <label for="address" class="form-label">ประเภท</label> 
                    
                     <select class="select2 form-select" name="type_id" id="type_id" required>
                                <option selected disabled>--กรุณาเลือกชนิดสินค้า--</option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['thj_type_prod_id']?>" ><?php echo $row_tp['thj_type_prod_name']?></option>
                                <?php } ?>
                            </select> 
                    </div> -->
                    <input type="hidden" name="tips_type" value="2">
                    <div class="mb-3 col-md-6">
                    <label for="state" class="form-label">ชื่อข่าว</label>
                    <input class="form-control" type="text" id="state" name="tips_title" placeholder="ชื่อข่าว">
                    </div>
                    <div class="mb-3 col-md-6">
                    <label for="zipCode" class="form-label">ลิงค์</label>
                    <input type="text" class="form-control" id="zipCode" name="tips_link" placeholder="ลิงค์">
                    </div>
                    <div class="mb-3 col-md-12">
                    <label for="zipCode" class="form-label">รายละเอียด</label>
                    <!-- <input type="text" class="form-control" id="zipCode" name="" > -->
                    <textarea name="tips_detail" class="form-control" placeholder="รายละเอียด" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="mt-2">
                    <button type="submit" class="btn btn-primary me-2">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
