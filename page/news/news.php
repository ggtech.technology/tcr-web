<?php

    $nquery2=mysqli_query($conn,"SELECT thj_tips_id,thj_tips.thj_tips_title,
    thj_tips.thj_tips_detail,
    thj_tips.thj_tips_link,thj_tips.thj_tips_date
    FROM thj_tips WHERE thj_tips_type=2");

    function httpPost1($url)
    {
        $curl_int = curl_init(); 
        curl_setopt($curl_int,CURLOPT_URL,$url);
        curl_setopt($curl_int,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl_int,CURLOPT_HEADER, false); 
        $output=curl_exec($curl_int);
        curl_close($curl_int);

        $obj = json_decode($output);
        // $type_id = $obj->type_id;
        // $amount = $obj->amount;
        // $discount = $obj->discount;
        $myJSON = json_encode($obj);
        
        return $myJSON;
        }

    // $d1=httpPost1('http://203.150.243.105/serve/etc/select_promotion_type');
    // echo $d1;
    $sql_tp="SELECT * FROM thj_type_product where thj_type_prod_status != 9";
        $qry_tp=mysqli_query($conn,$sql_tp);
?>
                <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก</span> / ข่าวสาร</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
    <div class="card-header d-flex justify-content-between">
        <h4></h4>
        <a href="" data-bs-toggle="modal" data-bs-target="#promo" class="btn btn-dark"><i class="fa-solid fa-plus"></i> ข่าวสาร</a> 
    </div>
  <div class="table-responsive text-nowrap">
  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>หัวข้อ</th>
                        <th>รายละเอียด</th>
                        <th>ลิงค์ข่าวสาร</th>
                        <th>วันที่</th>
                        <th>ลบ</th>
                      </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                        <?php $sum2=0; while($row = $nquery2->fetch_assoc()){ ?>
                                <td><b><?php echo $row['thj_tips_title']?></b></td>
                                <td><a href="" data-bs-toggle="modal" data-bs-target="#detail<?php echo $row["thj_tips_id"] ?>" class="text-success">อ่าน</a></td>
                                <td><a href="<?php echo $row['thj_tips_link'] ?>">เข้าชม</a></td>
                                <td><?php 
                                      $date = new DateTime($row['thj_tips_date']);
                                      $dm = $date->format('d-m-');
                                      $y = $date->format('Y')+543;
                                      echo $dm.$y;
                                    ?>
                                </td>
                                
                                <td><a href="" data-bs-toggle="modal" data-bs-target="#delete<?php echo $row["thj_tips_id"] ?>"><i class="fa-solid fa-trash text-danger"></i></a></td>
                            </tr>
                    <div class="modal fade" id="detail<?php echo $row['thj_tips_id'] ?>" tabindex="-1" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="modalScrollableTitle"><b><?php echo $row['thj_tips_title']?></b></h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="mb-3 col-md-5"><?php echo $row['thj_tips_detail'] ?></div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                                Close
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>

                            <div class="modal fade" id="delete<?php echo $row['thj_tips_id'] ?>" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="modalCenterTitle"><b>ยืนยันการลบ</b></h5>
                                    <button
                                        type="button"
                                        class="btn-close"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                    ></button>
                                    </div>
                                    <div class="modal-body" align="center">
                                        <div class="row" >
                                            <i class="fa-solid fa-circle-xmark text-danger" style="font-size: 5rem;"></i>
                                            <h4 class="mt-3"> คุณต้องการลบใช่หรือไม่ ?</h4>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    <form action="page/news/de_news.php" method="post">
                                        <input type="text" name="thj_tips_id" id="" value="<?php echo $row["thj_tips_id"] ?>" hidden>
                                        <button type="submit" class="btn btn-outline-danger">ลบ</button>
                                    </form>
                                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal"> ยกเลิก </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php } ?>
                    </tbody>
                  </table>
</div>
<!--/ Basic Bootstrap Table -->
        </div>
        <?php  include('in_news.php'); ?>

                        
        