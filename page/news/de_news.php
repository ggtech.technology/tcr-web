<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</style>
</head>
<body>
<?php
    require("../../src/conn.php");

    $id=$_POST["thj_tips_id"];
    
    $sql="DELETE FROM `thj_tips` WHERE thj_tips.thj_tips_id= " . $id;
    
    mysqli_set_charset($conn, 'utf8');
    if(mysqli_query($conn,$sql)){
        echo "<script type=\"text/javascript\">";
                echo "Swal.fire({
                    // position: 'top-end',
                    icon: 'success',
                    title: 'ลบรายการสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                    })";
                header("Refresh:1.5; url=../../admin.php?page=news");
                echo "</script>";
        exit();
     } 
     else{
        // echo mysqli_error($conn);
        echo "<script type=\"text/javascript\">";
                echo "Swal.fire({
                    // position: 'top-end',
                    icon: 'error',
                    title: 'ลบรายการไม่สำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                    })";
                header("Refresh:1.5; url=../../admin.php?page=news");
                echo "</script>";
        exit();
     }

?>
</body>
</html>