    <div class="mt-3">
        <?php
            $nquery1=mysqli_query($conn,"SELECT * FROM thj_vip_proof 
            INNER JOIN thj_account ON thj_vip_proof.thj_vip_proof_user_id=thj_account.thj_acc_id ".$like.$form.$to."");

            while($rows = $nquery1->fetch_assoc()){ ?>
        <!-- Modal -->
        <div class="modal fade" id="modalCenter4<?php echo $rows["thj_vip_proof_id"] ?>" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-l" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $rows['thj_acc_name']?></b></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" ></button>
                </div>
                <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                    <div class="card table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>โทรศัพท์</th>
                            <td><?php 
                                    if($rows['thj_acc_tel']!=''){
                                        $tel=str_split($rows['thj_acc_tel']);
                                        echo $tel[0].$tel[1].$tel[2].'-'.$tel[3].$tel[4].$tel[5].' '.$tel[6].$tel[7].$tel[8].$tel[9];
                                        } 
                            ?></td>
                        </tr>
                        <tr>
                            <th>หมายเลขบัตรประชาชน</th>
                            <!-- <td><img class="img-thumbnail img-fluid rounded mx-auto d-block" src="<?php echo $rows["thj_acc_id_card"] ?>" alt="" ></td> -->
                            <td><?php echo $rows["thj_acc_number_card"] ?></td>
                        </tr>
                        <tr>
                            <th>วันที่สมัคร</th>
                            <td><?php
                                        $date = new DateTime($rows['thj_vip_proof_date']);
                                        $dm = $date->format('d-m-');
                                        $y = $date->format('Y')+543;
                                        echo $dm.$y;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>วันหมดอายุ</th>
                            <td><?php
                                        $dates = new DateTime($rows['thj_vip_proof_dEx']);
                                        $dm1 = $dates->format('d-m-');
                                        $y1 = $dates->format('Y')+543;
                                        echo $dm1.$y1;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>คำเชิญ</th>
                            <td><?php echo $rows["thj_acc_code_saler"] ?></td>
                        </tr>
                        <tr>
                        <th>สถานะ</th>
                        <td> 
                            <?php
                            $sta="";
                                if($rows['thj_vip_proof_status'] == 'vip'){
                                echo '<span class="badge bg-label-success me-1">สมาชิก</span>';
                                $sta='<button class="btn btn-dark" disabled><i class="fa-solid fa-check"></i></button>';
                                }
                                elseif($rows['thj_vip_proof_status'] == 'expried'){
                                echo '<span class="badge bg-label-danger me-1">หมดอายุ</span>';
                                $sta='<button class="btn btn-dark" disabled><i class="fa-solid fa-check"></i></button>';
                                }
                                elseif($rows['thj_vip_proof_status'] == 'request'){
                                echo '<span class="badge bg-label-primary me-1">รอยืนยัน</span>';
                                $sta='<a href="" data-bs-toggle="modal" data-bs-target="#approve'.$row['thj_vip_proof_id'].'"><button class="btn btn-success" ><i class="fa-solid fa-check"></i></button></a>';
                                }
                            ?>
                            </td>
                        </tr>
                        </thead>
                    </table>
                    </div>
                    </div></div>
                    </div>
                </div>
                </div>
            </div>
            <?php } ?>
        </div>