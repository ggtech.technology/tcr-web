    <?php 
        require("src/conn.php");
        mysqli_query($conn,"SET CHARACTER SET UTF8"); 
        $sql_ma="SELECT * FROM thj_major where thj_major_user_id=".$row_user["thj_acc_id"];
        $qry_ma = $conn -> query($sql_ma);
        $maa = $qry_ma -> fetch_assoc();

        $sql_tp="SELECT * FROM thj_major where thj_major_user_id != ".$row_user["thj_acc_id"];
        $qry_tp=mysqli_query($conn,$sql_tp);

        $sql_am="SELECT * FROM amphures";
        $qry_am=mysqli_query($conn,$sql_am);

        $sql_dis="SELECT * FROM districts";
        $qry_dis=mysqli_query($conn,$sql_dis);
    ?>
    <script>
      $(document).ready(function(){
			$("#type_id").change(function(){
				var type_id = $(this).val();
				$.ajax({
					url: 'page/pickup_major/data/data.php',
					method: 'post',
					data: {id:type_id,function:'type_id'},
          success: function(data){
            console.log(data);
            $('#type_id2').html(data);
            // console.log(data);
          }
				})
				})
			})
    </script>
       <!-- Content wrapper -->
       <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> <span class="text-muted fw-light">เบิกสินค้า /</span> เบิกสินค้า</h4>

              <div class="row">
                <div class="col-md-12">
                  <div class="card mb-2">
                    <h4 class="card-header">เบิกสินค้า</h4>
                    <!-- Account -->
                    <hr class="my-0" />
                    <div class="card-body">
                      <form action="page/pickup_major/data/in_pi_ma.php" method="post" >
                        <div class="row">
                          <div class="mb-3 col-md-6">
                            <label for="firstName" class="form-label">เบิกจากสาขา</label>
                            <!-- <input
                              class="form-control"
                              type="text"
                              id="firstName"
                              name="order_fr_mid"
                              placeholder="ชื่อสาขา"
                              autofocus
                              required
                            /> -->
                            <select class="select2 form-select" name="order_fr_mid" id="type_id" required>
                                <option selected disabled>--กรุณาเลือกสาขา--</option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['thj_major_id']?>" ><?php echo $row_tp['thj_major_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">มายังสาขา</label>
                            <input class="form-control" type="text" name="order_to_mid" value="<?php echo $maa["thj_major_name"]?>" required id="lastName" disabled/>
                            <input class="form-control" type="text" name="order_to_mid" value="<?php echo $maa["thj_major_id"]?>" required id="lastName" hidden/>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">สินค้า</label>
                            <!-- <input class="form-control" type="text" name="major_tel" placeholder="สินค้า" required id="firstTr" /> -->
                        <table class="table" id="myTbl">
                            <thead>
                            </thead>
                            <tbody > 
                                <tr id="firstTr">
                                  <td>
                                            <select class="select2 form-select" name="prod[]" id="type_id2">
                                            </select>
                                  </td>
                                  <td><input class="form-control" type="text" name="num[]" placeholder="จำนวนสินค้า" required />
                                  </td>
                                </tr>
                            </tbody>
                        </table>
                          </div>
                          <div class="mb-3 col-md-6">
                          <label for="lastName" class="form-label"></label><br>
                          <button id="addRow" class="btn btn-success add-new" type="button"><i class="fa fa-plus"></i></button>
                          <button id="removeRow" class="btn btn-danger add-new" type="button"><i class="fa fa-delete"></i>-</button>
                        </div>
                        </div>
                        <div class="mt-2">
                          <button type="submit" name="submit" class="btn btn-primary me-2">บันทึก</button>
                          <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                        </div>
                      </form>
                    </div>
                    <!-- /Account -->
                  </div>
                </div>
              </div>
            </div>
            <!-- / Content -->
            <script src="https://code.jquery.com/jquery-1.11.1.js"></script>
            <script type="text/javascript">
            $(function(){
              $("#addRow").click(function(){
                  $("#myTbl").append($("#firstTr").clone());
              });
              $("#removeRow").click(function(){
                  if($("#myTbl tr").length>1){
                      $("#myTbl tr:last").remove();
                  }else{
                      alert("ต้องมีสินค้าอย่างน้อย 1 ชิ้น");
                  }
              });           
            });
            </script>