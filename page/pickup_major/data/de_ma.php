<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</style>
</head>
<body>
<?php
    require("../../../src/conn.php");

    $id=$_POST["major_order_id"];
    
    $sql="DELETE FROM `thj_major_order` WHERE thj_major_order.thj_major_order_id= " . $id;
    
    mysqli_set_charset($conn, 'utf8');
    if(mysqli_query($conn,$sql)){
        echo "<script type=\"text/javascript\">";
                echo "Swal.fire({
                    // position: 'top-end',
                    icon: 'success',
                    title: 'ลบรายการสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                    })";
                header("Refresh:1.5; url=../../../major.php?page=pickup_major");
                echo "</script>";
        exit();
     } 
     else{
        // echo mysqli_error($conn);
        echo "<script type=\"text/javascript\">";
                echo "Swal.fire({
                    // position: 'top-end',
                    icon: 'error',
                    title: 'ลบรายการไม่สำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                    })";
                header("Refresh:1.5; url=../../../major.php?page=pickup_major");
                echo "</script>";
        exit();
     }

?>
</body>
</html>