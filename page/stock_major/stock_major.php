<?php
    // session_start();
    $id = $_SESSION['id'] ;

    $query=mysqli_query($conn,"SELECT COUNT(thj_major_id) FROM `thj_major`");
    $row = mysqli_fetch_row($query);
    
    $rows = $row[0];
    
    $page_rows = 10;  //จำนวนข้อมูลที่ต้องการให้แสดงใน 1 หน้า  ตย. 10 record / หน้า 
    
    $last = ceil($rows/$page_rows);
    
    if($last < 1){
      $last = 1;
    }
    
    $pagenum = 1;
    
    if(isset($_GET['pn'])){
      $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
    }
    
    if ($pagenum < 1) {
      $pagenum = 1;
    }
    else if ($pagenum > $last) {
      $pagenum = $last;
    }
    
   
    
    $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
    
    $nquery=mysqli_query($conn,"SELECT * 
    FROM thj_major 
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id ");

    // $nquery1=mysqli_query($conn,"SELECT provinces.name_th as proname,amphures.name_th as amname,districts.name_th as disname,thj_major_id,thj_major_name,thj_acc_name,thj_major_tel
    // FROM thj_major
    // LEFT JOIN provinces ON thj_major.thj_major_province_id = provinces.id
    // LEFT JOIN amphures ON thj_major.thj_major_amphures_id = amphures.id
    // LEFT JOIN districts ON thj_major.thj_major_district_id = districts.id
    // LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id $limit");
    
    $paginationCtrls = '';
    
//     if($last != 1){
    
//     if ($pagenum > 1) {
//           $previous = $pagenum - 1;
//                   $paginationCtrls .= '
//                   <nav aria-label="Page navigation">
//                           <ul class="pagination">
//                             <li class="page-item prev">
//                               <a class="page-link" href="admin.php?page=major&pn='.$previous.'"><i class="tf-icon bx bx-chevron-left"></i></a>
//                             </li>
//                   ';
          
//                   for($i = $pagenum-4; $i < $pagenum; $i++){
//                       if($i > 0){
//                   $paginationCtrls .= '
//                     <li class="page-item">
//                         <a class="page-link" href="admin.php?page=major&pn='.$i.'">'.$i.'</a>
//                     </li>
//                     ';
//                       }
//               }
//           }
          
//               $paginationCtrls .= '
//               <li class="page-item active">
//                 <a class="page-link" href="">'.$pagenum.'</a>
//              </li>
//               ';
          
//               for($i = $pagenum+1; $i <= $last; $i++){
//                   $paginationCtrls .= '
//                     <li class="page-item">
//                         <a class="page-link" href="admin.php?page=major&pn='.$i.'">'.$i.'</a>
//                     </li>';
//                   if($i >= $pagenum+4){
//                       break;
//                   }
//               }
          
//           if ($pagenum != $last) {
//           $next = $pagenum + 1;
//           $paginationCtrls .= '
//           <li class="page-item next">
//           <a class="page-link" href="admin.php?page=major&pn='.$next.'"><i class="tf-icon bx bx-chevron-right"></i></a>
//             </li>
//         </ul> </nav>

//            ';
//           }
//               }
// ?>
                <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> สาขา</h4>

<!-- Basic Bootstrap Table -->
<!-- <div class="card">
    <div class="card-header d-flex justify-content-between">
        <h4></h4>
        <a href="./admin.php?pages=major&func=insert_major" class="btn btn-dark"><i class="fa-solid fa-plus"></i> เพิ่มสาขา</a>
    </div> -->
<!--     
  <form action="" class="form-inline mb-2 ms-2 me-2 " method="get" align="center">
              <div class="input-group mr-2 mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text">ชื่อสาขา</div>
                </div>
                <input type="text" name="" id="" class="form-control " placeholder="ชื่อสาขา">
              </div> 
              <input type="submit" name="submit" value="ค้นหา" class="btn btn-dark mb-2">
  </form> -->
  <div class="table-responsive text-nowrap">
    <table class="table table-striped">
    <caption class="ms-4">
                  <div id="pagination_controls" style="display: flex;">
                    <div class="pagination">
                        <?php echo $paginationCtrls; ?>
                    </div>
  </div>
                    </caption>
      <thead>
        <tr>
            <th>สาขา</th>
            <th>ชื่อผู้จัดการ</th>
            <th>โทรศัพท์</th>
            <th>ดูสต๊อกสินค้า</th>
           
        </tr>
      </thead>
      <?php 
            require("src/conn.php");
            
            mysqli_query($conn,"SET CHARACTER SET UTF8"); 
            while($row = $nquery->fetch_assoc()){
              // if($row["thj_major_id"] == )
              if($id == $row['thj_major_user_id']){
        ?> 
      <tbody class="table-border-bottom-0">
        <tr>
          <td><!-- <i class="fab fa-angular fa-lg text-danger me-3"></i> --> <strong><?php echo $row['thj_major_name']?></strong></td>
          <td><?php echo $row['thj_acc_name']?></td>
          <td><?php echo $row['thj_major_tel']?></td>

          <td><a href="./major.php?pages=stock_major&func=detail_stock_major&major_id=<?php echo$row["thj_major_id"] ?>"><i class="fa-solid fa-eye text-success"></i></a></td>
        </tr>
      </tbody>
      <?php }else{

      }} ?>
    </table>
</div>

        </div>
