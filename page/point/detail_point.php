<div class="mt-3">
                      <?php while($rows = $nquery1->fetch_assoc()){ ?>
                        <!-- Modal -->
                        <div class="modal fade" id="modalCenter1<?php echo $rows["thj_acc_id"] ?>" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $rows['thj_acc_name']?></b></h5>
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-sm-5">
                                    <img class="img-thumbnail img-fluid rounded mx-auto d-block" src="<?php echo $rows['thj_exchange_img_proof']?>" alt="" >
                                  </div>
                                  <div class="col-sm-7">
                                  <div class="card table-responsive">
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th>ชื่อสินค้า</th>
                                          <td><?php echo $rows["thj_acc_name"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ชื่อผู้ใช้งาน</th>
                                          <td><?php echo $rows["thj_acc_email_user"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>วันที่-เวลา</th>
                                          <td><?php echo $rows["thj_exchange_date"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>คะแนน</th>
                                          <td><?php echo $rows["thj_acc_id_point"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>สถานะ</th>
                                          <td>
                                            <?php
                                                    if($rows['thj_exchange_status'] == '1'){
                                                    echo '<a href="" data-bs-toggle="modal" data-bs-target="#modalCenter2'. $rows['thj_acc_id'].'"><span class="badge bg-label-success me-1">สำร็จ</span></a>';
                                                    }
                                                    else{
                                                    echo '<a href="" data-bs-toggle="modal" data-bs-target="#modalCenter2'. $rows['thj_acc_id'].'"><span class="badge bg-label-primary me-1">รอดำเนินการ</span></a>&nbsp;&nbsp;';
                                                    }
                                                ?>
                                          </td>
                                        </tr>
                                      </thead>
                                    </table>
                                    </div>
                                    </div></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <?php } ?>
                        </div>