<?php 
    if(isset($_GET["page"])){
        $page = $_GET["page"];
        if($page == "product_major"){
          $product = "active";
        }
        elseif($page == "stock_major"){
          $stock_major = "active";
        }
        elseif($page == "detail_stock_major"){
          $stock_major = "active";
        }
        elseif($page == "bell"){
          $bell = "active";
        }
        elseif($page == "member"){
          $member = "active";
        }
        elseif($page == "usertie"){
          $usertie = "active";
        }
        elseif($page == "major"){
          $major = "active";
        }
        elseif($page == "order_major"){
          $order_major = "active";
        }
        elseif($page == "point"){
          $point = "active";
        }
        elseif($page == "support"){
          $support = "active";
        }
        elseif($page == "doc"){
          $doc = "active";
        }
        elseif($page == "news"){
          $news = "active";
          $soci = "active";
        }
        elseif($page == "promo"){
          $promo = "active";
          $soci = "active";
        }
        elseif($page == "vdo"){
          $vdo = "active";
          $soci = "active";
        }
        elseif($page == "post"){
          $post = "active";
          $soci = "active";
        }
    }
    elseif(isset($_GET["pages"])){
      $pages = $_GET["pages"];
      if($pages == "product"){
        $product = "active";
      }
    }
    else{
      $major = "active";
    }
?>
         <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
          <div class="app-brand demo">
            <a href="major.php" class="app-brand-link">
              <span class="app-brand-text demo menu-text fw-bolder ms-2"><img src="assets/img/logo.png" alt="" style="width: 4rem;height: 2.5rem;"> TCR Online</span>
            </a>

            <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
              <i class="bx bx-chevron-left bx-sm align-middle"></i>
            </a>
          </div>

          <div class="menu-inner-shadow"></div>

          <ul class="menu-inner py-1">
            <!-- Dashboard -->
            <li class="menu-item <?php echo $major ?>">
              <a href="major.php" class="menu-link">
                <i class="menu-icon tf-icons fa-solid fa-house"></i>
                <div data-i18n="Analytics">หน้าแรก</div>
              </a>
            </li>
            <li class="menu-header small text-uppercase">
              <span class="menu-header-text">เมนู</span>
            </li>
             <li class="menu-item <?php echo $stock_major ?>">
              <a href="major.php?page=stock_major" 
           class="menu-link">
                <i class="fa-sharp fa-solid 	fa fa-cubes menu-icon tf-icons"></i>
                <div data-i18n="Basic">สต๊อกสินค้า</div>
              </a>
            </li>
            <!--<li class="menu-item <?php echo $product ?>">
              <a href="major.php?page=product_major" class="menu-link">
                <i class="fa-solid fa-database menu-icon tf-icons"></i>
                <div data-i18n="Basic">สินค้า</div>
              </a>
            </li>


             <li class="menu-item <?php echo $stock ?>">
              <a href="major.php?page=pickup_major" class="menu-link">
                <i class="fa-sharp fa-solid fa-truck-fast menu-icon tf-icons"></i>
                <div data-i18n="Basic">เบิกสินค้า</div>
              </a>
            </li>
             <li class="menu-item <?php echo $bell ?>">
              <a href="major.php?page=bell" class="menu-link">
                <i class="fa-solid fa-bell menu-icon tf-icons"></i>
                <div data-i18n="Basic">คำร้องของเบิกสินค้า</div>
              </a>
            </li> -->
            <!--<li class="menu-item <?php echo $member ?>">
              <a href="admin.php?page=member" class="menu-link">
                <i class="fa-solid fa-users menu-icon tf-icons"></i>
                <div data-i18n="Basic">ข้อมูลสมาชิก</div>
              </a>
            </li>
            <li class="menu-item <?php echo $usertie ?>">
              <a href="admin.php?page=usertie" class="menu-link">
                <i class="fa-solid fa-user-tie menu-icon tf-icons"></i>
                <div data-i18n="Basic">ข้อมูลเซลล์แมน</div>
              </a>
            </li>
            <li class="menu-item <?php echo $major ?>">
              <a href="admin.php?page=major" class="menu-link">
                <i class="fa-solid fa-shop menu-icon tf-icons"></i>
                <div data-i18n="Basic">ข้อมูลสาขา</div>
              </a>
            </li> -->
            <!-- <li class="menu-item <?php echo $order_major ?>">
              <a href="major.php?page=order_major" class="menu-link">
                <i class="fa-solid fa-file-lines menu-icon tf-icons"></i>
                <div data-i18n="Basic">ออเดอร์</div>
              </a>
            </li> -->
            <!-- <li class="menu-header small text-uppercase"><span class="menu-header-text">โซเชียลมีเดีย</span></li>
            <li class="menu-item <?php echo $soci ?>">
              <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons fa-solid fa-globe"></i>
                <div data-i18n="Authentications">โซเชียลมีเดีย</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo $news ?>">
                  <a href="major.php?page=news" class="menu-link">
                    <div data-i18n="Basic">ข่าวสาร</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $promo ?>">
                  <a href="major.php?page=promo" class="menu-link">
                    <div data-i18n="Basic">โปรโมชั่น</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $vdo ?>">
                  <a href="major.php?page=vdo" class="menu-link">
                    <div data-i18n="Basic">วีดีโอ</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $post ?>">
                  <a href="major.php?page=post" class="menu-link">
                    <div data-i18n="Basic">เกล็ดความรู้</div>
                  </a>
                </li>
              </ul>
            </li> -->
            <li class="menu-header small text-uppercase"><span class="menu-header-text">การตั้งค่า</span></li>
            <li class="menu-item <?php echo $support ?>">
              <a
                href="major.php?page=support"
                target="_blank"
                class="menu-link"
              >
                <i class="menu-icon tf-icons bx bx-support"></i>
                <div data-i18n="Support">ช่วยเหลือ</div>
              </a>
            </li>
            <li class="menu-item <?php echo $doc ?>">
              <a
                href="major.php?page=doc"
                target="_blank"
                class="menu-link"
              >
                <i class="menu-icon tf-icons bx bx-file"></i>
                <div data-i18n="Documentation">วิธีการใช้งาน</div>
              </a>
            </li>
          </ul>
        </aside>
        <!-- / Menu -->