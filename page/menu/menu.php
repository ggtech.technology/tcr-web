<?php 
    include("menu_active.php");
?>
         <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
          <div class="app-brand demo">
            <a href="admin.php" class="app-brand-link">
              <span class="app-brand-text demo menu-text fw-bolder ms-2"><img src="assets/img/logo.png" alt="" style="width: 4rem;height: 2.5rem;"> TCRP Online</span>
            </a>

            <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
              <i class="bx bx-chevron-left bx-sm align-middle"></i>
            </a>
          </div>

          <div class="menu-inner-shadow"></div>

          <ul class="menu-inner py-1">
            <!-- Dashboard -->
            <li class="menu-item <?php echo $admin ?>">
              <a href="admin.php" class="menu-link">
                <i class="menu-icon tf-icons fa-solid fa-house"></i>
                <div data-i18n="Analytics">หน้าแรก</div>
              </a>
            </li>
            
            <li class="menu-header small text-uppercase">
              <span class="menu-header-text">เมนู</span>
            </li>
            <li class="menu-item <?php echo $product ?>">
              <a href="admin.php?page=product" class="menu-link">
                <i class="fa-solid fa-database menu-icon tf-icons"></i>
                <div data-i18n="Basic">สินค้า</div>
              </a>
            </li>
            <li class="menu-item <?php echo $stock ?>">
              <a href="admin.php?page=stock" class="menu-link">
                <i class="fa-sharp fa-solid 	fa fa-cubes menu-icon tf-icons"></i>
                <div data-i18n="Basic">สต๊อกสินค้า</div>
              </a>
            </li>
            <li class="menu-item <?php echo $user ?>">
              <a href="admin.php?page=user" class="menu-link">
                <i class="fa-solid fa-user menu-icon tf-icons"></i>
                <div data-i18n="Basic">บุคคลทั่วไป</div>
              </a>
            </li>
            <li class="menu-item <?php echo $member ?>">
              <a href="admin.php?page=member" class="menu-link">
                <i class="fa-solid fa-users menu-icon tf-icons"></i>
                <div data-i18n="Basic">สมาชิก</div>
              </a>
            </li>
            <li class="menu-item <?php echo $usertie ?>">
              <a href="admin.php?page=usertie" class="menu-link">
                <i class="fa-solid fa-user-tie menu-icon tf-icons"></i>
                <div data-i18n="Basic">นักธุรกิจ</div>
              </a>
            </li>
            <li class="menu-item <?php echo $major ?>">
              <a href="admin.php?page=major" class="menu-link">
                <i class="fa-solid fa-shop menu-icon tf-icons"></i>
                <div data-i18n="Basic">สาขา</div>
              </a>
            </li>
            <li class="menu-item <?php echo $inv_mj ?>">
              <a href="admin.php?page=inv_mj" class="menu-link">
                <i class="fa-solid fa-house-laptop menu-icon tf-icons"></i>
                <div data-i18n="Basic">สต็อกสาขา</div>
              </a>
            </li>
            <li class="menu-item <?php echo $or ?>">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
            <i class="fa-solid fa-file-lines menu-icon tf-icons"></i>
                <div data-i18n="Authentications">คำสั่งซื้อ</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo $order ?> ">
                  <a href="admin.php?page=order" class="menu-link">
                    <div data-i18n="Basic">คำสั่งซื้อ (รออนุมัติ)</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $order_search ?>">
                  <a href="admin.php?page=order_search" class="menu-link">
                    <div data-i18n="Basic">คำสั่งซื้อ (ค้นหา)</div>
                  </a>
                </li>
              </ul>
            </li>
            <li class="menu-item <?php echo $point ?>">
              <a href="admin.php?page=point" class="menu-link">
                <i class="fa-solid fa-circle-dollar-to-slot menu-icon tf-icons"></i>
                <div data-i18n="Basic">แลกคะแนน (รออนุมัติ)</div>
              </a>
            </li>
            <!-- <li class="menu-item <?php echo $history_buy ?>">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
            <i class="fa-solid fa-file-lines menu-icon tf-icons"></i>
                <div data-i18n="Authentications">ประวัติการซื้อ</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo $history_user ?>">
                  <a href="admin.php?page=history_user" class="menu-link">
                    <div data-i18n="Basic">ลูกค้า</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $history_member ?>">
                  <a href="admin.php?page=history_member" class="menu-link">
                    <div data-i18n="Basic">สมาชิก</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $history_seller ?>">
                  <a href="admin.php?page=history_seller" class="menu-link">
                    <div data-i18n="Basic">เซลล์</div>
                  </a>
                </li>
              </ul>
            </li>ๅ -->
            <!-- <li class="menu-item <?php echo $history ?>">
              <a href="admin.php?page=history" class="menu-link">
                <i class="fa-solid fa-clock-rotate-left menu-icon tf-icons"></i>
                <div data-i18n="Basic">ประวัติการแลกคะแนน</div>
              </a>
            </li> -->
            
            <li class="menu-item <?php echo $promo ?>">
                  <a href="admin.php?page=promo" class="menu-link">
                  <i class="fa-solid fa-receipt menu-icon tf-icons"></i>
                    <div data-i18n="Basic">โปรโมชั่น</div>
                  </a>
                </li>

            <li class="menu-header small text-uppercase"><span class="menu-header-text">รายงาน</span></li>
            <li class="menu-item <?php echo $buy ?>">
              <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class='menu-icon tf-icons bx bxs-shopping-bags'></i>
                <div data-i18n="Authentications">รายงานขายสินค้า</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo $bil ?>">
                  <a href="admin.php?page=bil" class="menu-link">
                    <div data-i18n="Basic">สรุปยอดขายตามบิล (ไม่แสดงรายละเอียด)</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $bil_detail ?>">
                  <a href="admin.php?page=bil_detail" class="menu-link">
                    <div data-i18n="Basic">สรุปยอดการขายตามบิล (แสดงรายละเอียด)</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $biluser ?>">
                  <a href="admin.php?page=biluser" class="menu-link">
                    <div data-i18n="Basic">รายงานแสดงข้อมูลสรุปยอดขายตามลูกค้า</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $biluserdetail ?>">
                  <a href="admin.php?page=biluserdetail" class="menu-link">
                    <div data-i18n="Basic">รายงานแสดงรายละเอียดการขายตามลูกค้า</div>
                  </a>
                </li>
              </ul>
            </li>
            <li class="menu-item <?php echo $sroce ?>">
              <a href="javascript:void(0);" class="menu-link menu-toggle">
                <!-- <i class="menu-icon tf-icons fa-solid fa-globe"></i> -->
                <i class='menu-icon tf-icons bx bxs-dollar-circle'></i>
                <div data-i18n="Authentications">คะแนน</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo $history_acc ?>">
                  <a href="admin.php?page=history_acc" class="menu-link">
                    <div data-i18n="Basic">ประวัติการได้คะแนน</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $history ?>">
                  <a href="admin.php?page=history" class="menu-link">
                    <div data-i18n="Basic">ประวัติการแลกคะแนน</div>
                  </a>
                </li>
              </ul>
            </li>

            <li class="menu-header small text-uppercase"><span class="menu-header-text">ข้อมูลพื้นฐาน</span></li>
            <li class="menu-item <?php echo $be ?>">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
            <i class="fa-solid fa-filter menu-icon tf-icons"></i>
                <div data-i18n="Authentications">ข้อมูลพื้นฐาน</div>
              </a>
              <ul class="menu-sub">
              <li class="menu-item <?php echo $type1 ?>">
              <a href="admin.php?page=type1" class="menu-link">
                <div data-i18n="Basic">ประเภทที่ 1</div>
              </a>
            </li><li class="menu-item <?php echo $type2 ?>">
              <a href="admin.php?page=type2" class="menu-link">
                <div data-i18n="Basic">ประเภทที่ 2</div>
              </a>
            </li><li class="menu-item <?php echo $type3 ?>">
              <a href="admin.php?page=type3" class="menu-link">
                <div data-i18n="Basic">ประเภทที่ 3</div>
              </a>
            </li><li class="menu-item <?php echo $type4 ?>">
              <a href="admin.php?page=type4" class="menu-link">
                <div data-i18n="Basic">ประเภทที่ 4</div>
              </a>
            </li>
              </ul>
            </li>
            
            
            <li class="menu-header small text-uppercase"><span class="menu-header-text">โซเชียลมีเดีย</span></li>
            <li class="menu-item <?php echo $soci ?>">
              <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons fa-solid fa-globe"></i>
                <div data-i18n="Authentications">โซเชียลมีเดีย</div>
              </a>
              <ul class="menu-sub">
                <li class="menu-item <?php echo $news ?>">
                  <a href="admin.php?page=news" class="menu-link">
                    <div data-i18n="Basic">ข่าวสาร</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $vdo ?>">
                  <a href="admin.php?page=vdo" class="menu-link">
                    <div data-i18n="Basic">วีดีโอ</div>
                  </a>
                </li>
                <li class="menu-item <?php echo $post ?>">
                  <a href="admin.php?page=post" class="menu-link">
                    <div data-i18n="Basic">เกล็ดความรู้</div>
                  </a>
                </li>
              </ul>
            </li>
            <li class="menu-header small text-uppercase"><span class="menu-header-text">การตั้งค่า</span></li>
            <!-- <li class="menu-item <?php echo $support ?>">
              <a
                href="admin.php?page=support"
                target="_blank"
                class="menu-link"
              >
                <i class="menu-icon tf-icons bx bx-support"></i>
                <div data-i18n="Support">ช่วยเหลือ</div>
              </a>
            </li> -->
            <li class="menu-item <?php echo $doc ?>">
              <a
                href="admin.php?page=doc"
                target="_blank"
                class="menu-link"
              >
                <i class="menu-icon tf-icons bx bx-file"></i>
                <div data-i18n="Documentation">วิธีการใช้งาน</div>
              </a>
            </li>
          </ul>
        </aside>
        <!-- / Menu -->