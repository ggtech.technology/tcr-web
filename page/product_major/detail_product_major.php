<div class="mt-3">
                      <?php while($rows = $nquery1->fetch_assoc()){ ?>
                        <!-- Modal -->
                        <div class="modal fade" id="modalCenter<?php echo $rows["thj_subtype_prod_id"] ?>" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $rows['thj_subtype_prod_name']?></b></h5>
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-sm-5">
                                    <img src="<?php echo $rows["thj_subtype_prod_img"]?>" alt="" class="img-fluid">
                                  </div>
                                  <div class="col-sm-7">
                                  <div class="card table-responsive">
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th>ชื่อสินค้า</th>
                                          <td><?php echo $rows["thj_subtype_prod_name"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ประเภท</th>
                                          <td><?php echo $rows["thj_type_prod_name"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ประเภท</th>
                                          <td><?php echo $rows["thj_type2_product_name"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ประเภท</th>
                                          <td><?php echo $rows["thj_type3_product_name"] ?></td>
                                        </tr>
                                        <tr>
                                          <th class="ms-2">รายละเอียดสินค้า</th>
                                          <td><?php echo $rows["thj_subtype_prod_detail"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>จำนวน</th>
                                          <td><?php echo $rows["thj_subtype_prod_amount"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ขนาดสินค้า</th>
                                          <td><?php echo $rows["thj_subtype_prod_weight"].' '.$rows["thj_subtype_prod_unit"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>ราคาสินค้า</th>
                                          <td><?php echo $rows["thj_subtype_prod_price"] ?> บาท</td>
                                        </tr>
                                        <tr>
                                          <th >ราคาสินค้า(สมาชิก)</th>
                                          <td><?php echo $rows["thj_subtype_prod_price_VIP"] ?> บาท</td>
                                        </tr>
                                        <tr>
                                          <th>คะแนน</th>
                                          <td><?php echo $rows["thj_subtype_prod_point"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>วันที่-เวลา</th>
                                          <td><?php echo $rows["thj_subtype_prod_date"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>สถานะ</th>
                                          <td>
                                            <?php if($rows["thj_subtype_status_P"] == '1'){ ?>
                                                <span class="badge bg-label-primary me-1">เปิดการใช้งาน</span>
                                            <?php } 
                                            elseif($rows["thj_subtype_status_P"] == '0'){ ?>
                                                      <span class="badge bg-label-danger me-1">ปิดการใช้งาน</span>
                                            <?php } ?>
                                          </td>
                                        </tr>
                                      </thead>
                                    </table>
                                    </div>
                                    </div></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <?php } ?>
                        </div>