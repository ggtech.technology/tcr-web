
     <script type="text/javascript">
		$(document).ready(function(){
			$("#type_id").change(function(){
				var type_id = $(this).val();
				$.ajax({
					url: 'page/product/data/data.php',
					method: 'post',
					data: {id:type_id,function:'type_id'},
          success: function(data){
            console.log(data);
            $('#type_id2').html(data);
            // console.log(data);
          }
				})
				})
        $("#type_id2").change(function(){
				var type_id2 = $(this).val();
				$.ajax({
					url: 'page/product/data/data.php',
					method: 'post',
					data: {id:type_id2,function:'type_id2'},
          success: function(data){
            console.log(data);
            $('#type_id3').html(data);
          }
				})
				})
        $("#type_id3").change(function(){
				var type_id2 = $(this).val();
				$.ajax({
					url: 'page/product/data/data.php',
					method: 'post',
					data: {id:type_id2,function:'type_id3'},
          success: function(data){
            console.log(data);
            $('#type_id4').html(data);
          }
				})
				})
			})
	</script>
    <?php 
        require("src/conn.php");
        mysqli_query($conn,"SET CHARACTER SET UTF8"); 
        $sql_ma="SELECT * FROM thj_major where thj_major_user_id=".$row_user["thj_acc_id"];
        $qry_ma = $conn -> query($sql_ma);
        $maa = $qry_ma -> fetch_assoc();

        $sql_tp="SELECT * FROM thj_type_product";
        $qry_tp=mysqli_query($conn,$sql_tp);
    ?>

        <!-- Content wrapper -->
        <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> <span class="text-muted fw-light">สินค้า /</span> เพิ่มสินค้า</h4>

              <div class="row">
                <div class="col-md-12">
                  <div class="card mb-2">
                    <h4 class="card-header">เพิ่มสินค้า</h4>
                    <!-- Account -->
                    <hr class="my-0" />
                    <div class="card-body">
                      <form action="http://203.150.243.105/serve/Product/product_store" id="formAccountSettings" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <input
                              class="form-control"
                              type="text"
                              name="major_id"
                              placeholder="สาขา"
                              value="<?php echo $maa["thj_major_id"] ?>"
                              hidden
                            />
                          <div class="mb-3 col-md-6">
                            <label for="firstName" class="form-label">ชื่อสินค้า</label>
                            <input
                              class="form-control"
                              type="text"
                              id="firstName"
                              name="prod_name"
                              placeholder="ชื่อสินค้า"
                              autofocus
                              required
                            />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">รายละเอียด</label>
                            <textarea name="prod_detail" id="" cols="20" rows="5" placeholder="รายละเอียด..." class="form-control" required></textarea>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="zipCode" class="form-label">ประเภท</label>
                            <select class="select2 form-select" name="type_id" id="type_id" required>
                                <option selected disabled>--กรุณาเลือกชนิดสินค้า--</option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['thj_type_prod_id']?>" ><?php echo $row_tp['thj_type_prod_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ประเภท</label>
                            <select class="select2 form-select" name="type2_id" id="type_id2">
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ประเภท</label>
                            <select class="select2 form-select" name="type3_id" id="type_id3">
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ประเภท</label>
                            <select class="select2 form-select" name="type4_id" id="type_id4">
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">จำนวน</label>
                            <input class="form-control" type="text" name="prod_amount" placeholder="จำนวน" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">ขนาด</label>
                            <input class="form-control" type="text" name="prod_weight" placeholder="ขนาด" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="language" class="form-label">หน่วย</label>
                            <select name="prod_unit" id="language" class="select2 form-select" required>
                              <option selected disabled>--กรุณาเลือกหน่วย--</option>
                              <option value="กิโลกรัม">กิโลกรัม</option>
                              <option value="กรัม">กรัม</option>
                              <option value="มิลลิลิตร">มิลลิลิตร</option>
                              <option value="ลิตร">ลิตร</option>
                              <option value="ซีซี">ซีซี</option>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">แต้ม</label>
                            <input class="form-control" type="text" name="prod_point" placeholder="แต้ม" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">ราคา</label>
                            <input class="form-control" type="text" name="prod_price" placeholder="ราคา" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">ราคาสมาชิก</label>
                            <input class="form-control" type="text" name="prod_price_VIP" placeholder="ราคาสมาชิก" required id="lastName" />
                          </div>
                        </div>
                        <input type="file" id="file-input" name="prod_img" accept="image/png, image/jpeg" onchange="preview()" multiple required hidden>
                        <label for="file-input">
                            <i class="fas fa-upload"></i> &nbsp; กรุณาเลือกรูปภาพ
                        </label>
                        <p id="num-of-files">ไม่มีไฟล์รูปภาพ</p>
                        <div id="images"></div>
                        <div class="mt-2">
                          <button type="submit" name="submit" class="btn btn-primary me-2">บันทึก</button>
                          <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                        </div>
                      </form>
                    </div>
                    <!-- /Account -->
                  </div>
                </div>
              </div>
            </div>
            <!-- / Content -->



<script>
    let fileInput = document.getElementById("file-input");
    let imageContainer = document.getElementById("images");
    let numOfFiles = document.getElementById("num-of-files");

    function preview(){
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;

        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                figure.insertBefore(img,figCap);
            }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
        }
    }
</script>