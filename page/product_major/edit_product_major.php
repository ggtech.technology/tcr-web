<?php 
        require("src/conn.php");

        $prod_id=$_GET['prod_id'];

        mysqli_query($conn,"SET CHARACTER SET UTF8"); 
        $sql_ma="SELECT * FROM thj_major where thj_major_user_id=".$row_user["thj_acc_id"];
        $qry_ma = $conn -> query($sql_ma);
        $maa = $qry_ma -> fetch_assoc();

        $qry_prod = mysqli_query($conn,"SELECT *
        FROM thj_subtype_prod
        LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
        LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
        LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
        LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
        LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
        LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
        Where thj_subtype_prod_id=$prod_id ORDER BY thj_subtype_prod.thj_subtype_prod_name");
        $prod = $qry_prod -> fetch_assoc();

        $sql_tp="SELECT * FROM thj_type_product";
        $qry_tp=mysqli_query($conn,$sql_tp);

        $sql_tp2="SELECT * FROM thj_type2_product";
        $qry_tp2=mysqli_query($conn,$sql_tp2);
        $maa2 = $qry_tp2 -> fetch_assoc();

        $sql_tp3="SELECT * FROM thj_type3_product";
        $qry_tp3=mysqli_query($conn,$sql_tp3);
        $maa3 = $qry_tp3 -> fetch_assoc();

        $sql_tp4="SELECT * FROM thj_type4_product";
        $qry_tp4=mysqli_query($conn,$sql_tp4);
        $maa4 = $qry_tp4 -> fetch_assoc();

        $sql_ma0="SELECT * FROM thj_major where thj_major_user_id=".$row_user["thj_acc_id"];
        $qry_ma0 = $conn -> query($sql_ma0);
        $maa0 = $qry_ma0 -> fetch_assoc();
    ?>

        <!-- Content wrapper -->
        <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
                <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> <span class="text-muted fw-light">สินค้า /</span> แก้ไขสินค้า</h4>

              <div class="row">
                <div class="col-md-12">
                  <div class="card mb-2">
                    <div class="card-header d-flex justify-content-between">
                      <h4 class="card-header">แก้ไขสินค้า</h4>
                      <button class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#modalCenter<?php echo $prod_id ?>"><i class="fas fa-upload"></i> &nbsp; แก้ไขรูปภาพ</button>
                    </div>
                    <!-- Account -->
                    <hr class="my-0" />
                    <div class="card-body">
                      <form action="http://203.150.243.105/serve/Product/update_data_product" method="post">
                        <div class="row">
                        <input
                              class="form-control"
                              type="text"
                              name="major_id"
                              placeholder="สาขา"
                              value="<?php echo $maa0["thj_major_id"] ?>"
                              hidden
                            />
                        <input type="text" name="prod_id" value="<?php echo $prod_id ?>" hidden>
                          <div class="mb-3 col-md-6">
                            <label for="firstName" class="form-label">ชื่อสินค้า</label>
                            <input
                              class="form-control"
                              type="text"
                              id="firstName"
                              name="prod_name"
                              value="<?php echo $prod['thj_subtype_prod_name']?>"
                              required
                            />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">รายละเอียด</label>
                            <textarea name="detail" id="" cols="15" rows="5" placeholder="รายละเอียด..." class="form-control" required><?php echo $prod['thj_subtype_prod_detail']?></textarea>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="zipCode" class="form-label">ประเภท</label>
                            <select class="select2 form-select" name="type_id" id="type_id" required>
                                <option selected value="<?php echo $prod['thj_type_prod_id']?>"><?php echo $prod['thj_type_prod_name']?></option>
                                <?php while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['thj_type_prod_id']?>" ><?php echo $row_tp['thj_type_prod_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ประเภท</label>
                            <select class="select2 form-select" name="type2_id" id="type_id2">
                                <option selected value="<?php echo $prod['thj_type2_product_id']?>"><?php echo $prod['thj_type2_product_name']?></option>
                                <?php while($row_tp2=mysqli_fetch_array($qry_tp2)){ ?>
                                    <option value="<?php echo $row_tp2['thj_type2_product_id']?>" ><?php echo $row_tp2['thj_type2_product_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ประเภท</label>
                            <select class="select2 form-select" name="type3_id" id="type_id3">
                                <option selected value="<?php echo $prod['thj_type3_product_id']?>"><?php echo $prod['thj_type3_product_name']?></option>
                                <?php while($row_tp3=mysqli_fetch_array($qry_tp3)){ ?>
                                    <option value="<?php echo $row_tp3['thj_type3_product_id']?>" ><?php echo $row_tp3['thj_type3_product_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label class="form-label" for="country">ประเภท</label>
                            <select class="select2 form-select" name="type4_id" id="type_id4">
                                <option selected value="<?php echo $prod['thj_type4_product_id']?>"><?php echo $prod['thj_type4_product_name']?></option>
                                <?php while($row_tp4=mysqli_fetch_array($qry_tp4)){ ?>
                                    <option value="<?php echo $row_tp4['thj_type4_product_id']?>" ><?php echo $row_tp4['thj_type4_product_name']?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">จำนวน</label>
                            <input class="form-control" type="text" name="amount" value="<?php echo $prod['thj_subtype_prod_amount']?>" placeholder="จำนวน" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">ขนาด</label>
                            <input class="form-control" type="text" name="weight" placeholder="ขนาด" value="<?php echo $prod['thj_subtype_prod_weight']?>" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="language" class="form-label">หน่วย</label>
                            <select name="unit" id="language" class="select2 form-select" required>
                              <option selected value="<?php echo $prod['thj_subtype_prod_unit']?>"><?php echo $prod['thj_subtype_prod_unit']?></option>
                              <option value="กิโลกรัม">กิโลกรัม</option>
                              <option value="กรัม">กรัม</option>
                              <option value="มิลลิลิตร">มิลลิลิตร</option>
                              <option value="ลิตร">ลิตร</option>
                              <option value="ซีซี">ซีซี</option>
                            </select>
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">แต้ม</label>
                            <input class="form-control" type="text" name="point" placeholder="แต้ม"  value="<?php echo $prod['thj_subtype_prod_point']?>" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">ราคา</label>
                            <input class="form-control" type="text" name="price" placeholder="ราคา"  value="<?php echo $prod['thj_subtype_prod_price']?>" required id="lastName" />
                          </div>
                          <div class="mb-3 col-md-6">
                            <label for="lastName" class="form-label">ราคาสมาชิก</label>
                            <input class="form-control" type="text" name="VIP" placeholder="ราคาสมาชิก"  value="<?php echo $prod['thj_subtype_prod_price_VIP']?>" required id="lastName" />
                          </div>
                        </div>
                            
                        <div class="mt-2">
                          <button type="submit" name="submit" class="btn btn-success me-2">แก้ไขข้อมูล</button>
                          <!-- <button type="reset" class="btn btn-outline-secondary">Cancel</button> -->
                        </div>
                      </form>
                      
                    </div>
                    <!-- /Account -->
                  </div>
                </div>
              </div>
            </div>
                            
            </div>
            <!-- / Content -->

                    <div class="mt-3">
                        <!-- Modal -->
                        <div class="modal fade" id="modalCenter<?php echo $prod_id ?>" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="modalCenterTitle"><b><?php echo $prod['thj_subtype_prod_name']?></b></h5>
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <form action="http://203.150.243.105/serve/Product/update_img_product" method="post" enctype="multipart/form-data">
                                  <div class="row">
                                    <div class="d-grid gap-2 col-6 mx-auto" align='center' style="width: 300px;">
                                    <input
                              class="form-control"
                              type="text"
                              name="major_id"
                              placeholder="สาขา"
                              value="<?php echo $maa0["thj_major_id"] ?>"
                              hidden
                            />
                                      <input type="text" name="prod_id" value="<?php echo $prod_id ?>" hidden>
                                      <input type="file" id="file-input" name="image" accept="image/png, image/jpeg" onchange="preview()" multiple required hidden>
                                      <button class="btn btn-primary" ><label for="file-input"><i class="fas fa-upload"></i> &nbsp;กรุณาเลือกรูปภาพ</label></button>
                                    </div>
                                      <p id="num-of-files" align="center"></p>
                                      <div id="images"><img class="img-thumbnail img-fluid rounded mx-auto d-block" src="<?php echo $prod['thj_subtype_prod_img']?>" alt="" style="width: 400px;"></div>
                                      <div class="d-grid gap-2 col-6 mx-auto" align='center' style="width: 300px;">
                                        <button class="btn btn-success" type="submit" name="submit">แก้ไขรูปภาพ</button>
                                    </div>
                                  </div>
                                </form>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

  <script>
    let fileInput = document.getElementById("file-input");
    let imageContainer = document.getElementById("images");
    let numOfFiles = document.getElementById("num-of-files");

    function preview(){
        imageContainer.innerHTML = "";
        numOfFiles.textContent = `${fileInput.files.length} Files Selected`;

        for(i of fileInput.files){
            let reader = new FileReader();
            let figure = document.createElement("figure");
            let figCap = document.createElement("figcaption");
            figCap.innerText = i.name;
            figure.appendChild(figCap);
            reader.onload=()=>{
                let img = document.createElement("img");
                img.setAttribute("src",reader.result);
                img.setAttribute("class","img-thumbnail img-fluid rounded mx-auto d-block mb-3");
                img.setAttribute("style","width: 400px;");
                figure.insertBefore(img,figCap);
            }
            imageContainer.appendChild(figure);
            reader.readAsDataURL(i);
        }
    }
</script>