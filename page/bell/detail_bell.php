<div class="mt-3">
                      <?php while($rows = $nquery1->fetch_assoc()){ ?>
                        <!-- Modal -->
                        <div class="modal fade" id="modalCenter<?php echo $rows["thj_major_order_id"] ?>" tabindex="-1" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button
                                  type="button"
                                  class="btn-close"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                ></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-sm-6">
                                  <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th>รายการสินค้า</th>
                                          <th>จำนวน</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php 
                                                $order_list=explode(',',$rows["order_list"]);
                                                
                                                
                                                // echo $list[0]." ".$list[1];
                                                for($i=0; $i < count($order_list); $i++){
                                                  $list=explode('/',$order_list[$i]);
                                                  // print_r($list);
                                                  // for($j=0; $j < $i; $j++){
                                                    echo "<tr>
                                                            <td><b>".$list[0]."</b></td>
                                                            <td>".$list[1]."</td>
                                                        </tr>";
                                                  // }
                                                }
                                        ?>
                                        <tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="col-sm-6">
                                  <div class="card table-responsive">
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th>วัน-เวลาสั่ง</th>
                                          <td><?php echo $rows['order_date']?></td>
                                        </tr>
                                        <tr>
                                          <th>สาขาที่ขอเบิก</th>
                                          <td><?php echo $rows["order_from"] ?></td>
                                        </tr>
                                        <tr>
                                          <th>สถานะ</th>
                                          <td>
                                          <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                                            <?php if($rows["order_status"] == '2'){ ?>
                                              <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox" id="sta<?php echo $rows["thj_major_order_id"] ?>">
                                                  </div>
                                            <?php } 
                                            elseif($rows["order_status"] == '5'){ ?>
                                                  <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox" id="sta<?php echo $rows["thj_major_order_id"] ?>" checked>
                                                  </div>
                                            <?php } ?>
                                          </div>
                                        </td>
                                        </tr>
                                      </thead>
                                    </table>
                                    </div>
                                    </div></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <script>
                    $(function() {
                      $('#sta<?php echo $rows["thj_major_order_id"] ?>').change(function() {
                        var ch_val = $(this).prop('checked');
                        var rel = <?php echo $rows['thj_major_order_id']; ?>;

                        if(ch_val==true){
                          var status = 5;
                        }
                        if(ch_val==false){
                          var status = 2;
                        }
                        $.ajax({
                            url: 'page/bell/data/data.php',
                            type: 'POST',
                            data: {id: rel, value: status},
                            // async: false,
                            success: function (data) {
                              console.log(data);
                              // data = JSON.toString(data);
                              }
                          });
                    
                      })
                    })
                </script>
                          <?php } ?>
                        </div>