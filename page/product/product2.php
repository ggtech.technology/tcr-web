<?php

    if(isset($_GET["pdtype2"])){
      $pdtype2=$_GET["pdtype2"];
      $pdtype=$_GET["pdtype"];
      $type2="SELECT * FROM `thj_type3_product`
      LEFT JOIN thj_type2_product ON thj_type3_product.thj_type2_product_id = thj_type2_product.thj_type2_product_id
      LEFT JOIN thj_type_product ON  thj_type2_product.thj_type_product_id = thj_type_product.thj_type_prod_id
      where thj_type_prod_status != 9 and thj_type2_product.thj_type2_product_id=$pdtype2";
      $qry_t2=mysqli_query($conn,$type2);
    }
    else{
       
    }


    $nquery1=mysqli_query($conn,"SELECT *
    FROM thj_subtype_prod
    LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
    LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
    LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
    LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
    LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
    where thj_account.thj_acc_id=".$row_user["thj_acc_id"]." 
    ORDER BY thj_subtype_prod.thj_subtype_prod_name");
    
?>
                
        <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> สินค้า</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
    <div class="card-header d-flex justify-content-between">
        <h4></h4>
        <a href="./admin.php?pages=product&func=insert_product" class="btn btn-dark"><i class="fa-solid fa-plus"></i> เพิ่มสินค้า</a>
    </div>
    
  <form action="./admin.php?pages=product&func=prod_prod" class="form-inline mb-2 ms-2 me-2 " method="get" align="center">
              <input type="text" name="pages" value="product" hidden> 
              <input type="text" name="func" value="prod_prod" hidden> 
              <div class="row">
                  <div class="col-md-12">
                  <div class="input-group mr-2 mb-3 col-3">
                <div class="input-group-prepend">
                    <div class="input-group-text">ชื่อสินค้า</div>
                </div>
                <input type="text" name="name_pord" id="input" class="form-control " placeholder="ชื่อสินค้า">
              </div> 
                  </div>
                 
              </div>
              <input type="submit" name="submit" value="ค้นหา" class="btn btn-dark mb-2">
  </form>
<!--/ Basic Bootstrap Table -->
        </div> 
       <?php include('detail_product.php'); ?>

      <div class="constant">
        <?php while($row = mysqli_fetch_array($qry_t2)){ 
            if($pdtype == 3){
               $btn = "btn btn-outline-danger btn-xl";
               $icon = "fa-solid fa-biohazard";
            }
            else if($pdtype == 2){
                $btn ="btn btn-outline-dark btn-xl";
                $icon = "bx bx-recycle";
            }
            else{
                $btn ="btn btn-outline-warning btn-xl";
                $icon = "bx bx-spa";
            }
            ?>
            <a href="./admin.php?pages=product&func=prod_prod1&pdtype2=<?php echo $row["thj_type2_product_id"] ?>&pdtype3=<?php echo $row["thj_type3_product_id"] ?>"><button type="button" class="<?php echo $btn ?>"><i class="<?php echo $icon ?>"></i>&nbsp;&nbsp; <?php echo $row["thj_type3_product_name"]?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='bx bx-chevron-right'></i></button></a><br><br>
        <?php } ?>
        <button onclick="history.back()" class="btn btn-outline-secondary btn-xl" ><i class='bx bx-chevron-left'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; กลับ &nbsp;&nbsp;</button>
      </div>