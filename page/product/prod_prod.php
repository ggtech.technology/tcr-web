<?php
   
    $like="";
    $type="";
    if(isset($_GET["name_pord"])){
      $name_pord=$_GET["name_pord"];
      $like="AND thj_subtype_prod.thj_subtype_prod_name LIKE '%".$name_pord."%' ";
    }
    else{
      $type="";
      $like=""; 
    }

    $query=mysqli_query($conn,"SELECT COUNT(thj_subtype_prod_id) FROM `thj_subtype_prod`
    LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
    LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
    LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
    LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
    LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
     where thj_account.thj_acc_id=".$row_user["thj_acc_id"]." ".$like."  ");
    $row = mysqli_fetch_row($query);
    
    $rows = $row[0];
    
    $page_rows = 20;  //จำนวนข้อมูลที่ต้องการให้แสดงใน 1 หน้า  ตย. 10 record / หน้า 
    
    $last = ceil($rows/$page_rows);
    
    if($last < 1){
      $last = 1;
    }
    
    $pagenum = 1;
    
    if(isset($_GET['pn'])){
      $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
    }
    
    if ($pagenum < 1) {
      $pagenum = 1;
    }
    else if ($pagenum > $last) {
      $pagenum = $last;
    }
    
    $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
    
    $nquery=mysqli_query($conn,"SELECT *
    FROM thj_subtype_prod
    LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
    LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
    LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
    LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
    LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
    where thj_account.thj_acc_id=".$row_user["thj_acc_id"]." ".$like." 
    ORDER BY CONVERT( thj_subtype_prod.thj_subtype_prod_name USING tis620 ) ASC $limit");

    $nquery1=mysqli_query($conn,"SELECT *
    FROM thj_subtype_prod
    LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
    LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
    LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
    LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
    LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
    where thj_account.thj_acc_id=".$row_user["thj_acc_id"]." ".$like." 
    ORDER BY CONVERT( thj_subtype_prod.thj_subtype_prod_name USING tis620 ) ASC $limit");
    
    $paginationCtrls = '';
    
    if($last != 1){
    
    if ($pagenum > 1) {
          $previous = $pagenum - 1;
                  $paginationCtrls .= '
                  <nav aria-label="Page navigation">
                          <ul class="pagination">
                            <li class="page-item prev">
                              <a class="page-link" href="admin.php?page=product&pn='.$previous.'"><i class="tf-icon bx bx-chevron-left"></i></a>
                            </li>
                  ';
          
                  for($i = $pagenum-4; $i < $pagenum; $i++){
                      if($i > 0){
                  $paginationCtrls .= '
                    <li class="page-item">
                        <a class="page-link" href="admin.php?page=product&pn='.$i.'">'.$i.'</a>
                    </li>
                    ';
                      }
              }
          }
          
              $paginationCtrls .= '
              <li class="page-item active">
                <a class="page-link" href="">'.$pagenum.'</a>
             </li>
              ';
          
              for($i = $pagenum+1; $i <= $last; $i++){
                  $paginationCtrls .= '
                    <li class="page-item">
                        <a class="page-link" href="admin.php?page=product&pn='.$i.'">'.$i.'</a>
                    </li>';
                  if($i >= $pagenum+4){
                      break;
                  }
              }
          
          if ($pagenum != $last) {
          $next = $pagenum + 1;
          $paginationCtrls .= '
          <li class="page-item next">
          <a class="page-link" href="admin.php?page=product&pn='.$next.'"><i class="tf-icon bx bx-chevron-right"></i></a>
            </li>
        </ul> </nav>

           ';
          }
              }
?>
                
        <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> สินค้า</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
    <div class="card-header d-flex justify-content-between">
    <button onclick="history.back()" class="btn btn-secondary" ><i class='bx bx-chevron-left'></i>กลับ</button>
        <a href="./admin.php?pages=product&func=insert_product" class="btn btn-dark"><i class="fa-solid fa-plus"></i> เพิ่มสินค้า</a>
    </div>
    
  <form action="" class="form-inline mb-2 ms-2 me-2 " method="get" align="center">
              <input type="text" name="pages" value="product" hidden> 
              <input type="text" name="func" value="prod_prod" hidden> 
              <div class="row">
                  <div class="col-md-12">
                  <div class="input-group mr-2 mb-3 col-3">
                <div class="input-group-prepend">
                    <div class="input-group-text">ชื่อสินค้า</div>
                </div>
                <input type="text" name="name_pord" id="input" class="form-control " placeholder="ชื่อสินค้า">
              </div>
              <input type="submit" name="submit" value="ค้นหา" class="btn btn-dark mb-2">
              
  </form>
  
  <div class="table-responsive text-nowrap">
    <table class="table table-striped">
    <!-- <caption class="ms-4">
                  <div id="pagination_controls" style="display: flex;">
                    <div class="pagination">
                        <?php echo $paginationCtrls; ?>
                    </div>
  </div>
                    </caption> -->
      <thead>
        <tr>
            <th>ชื่อสินค้า</th>
            <th>ประเภท</th>
            <th>ราคา</th>
            <th>สถานะ</th>
            <th>ดูรายละเอียด</th>
            <th>แก้ไข</th>
            <!-- <th>การใช้งานสินค้า</th> -->
            <th>ลบ</th>
        </tr>
      </thead>
      <?php 
            require("src/conn.php");
            
            mysqli_query($conn,"SET CHARACTER SET UTF8"); 
            while($row = mysqli_fetch_array($nquery)){
        ?> 
      <tbody class="table-border-bottom-0">
        <tr>
          <td align="left"><strong><?php echo $row['thj_subtype_prod_name']?></strong></td>
          <td><?php echo $row['thj_type_prod_name'].' '.$row['thj_type2_product_name'].' '.$row['thj_type3_product_name']?></td>
          <td><?php echo $row['thj_subtype_prod_price']?> บาท</td>
          <td>
            <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
              <?php if($row["thj_subtype_status_P"] == '1'){ ?>
                <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="sta<?php echo $row["thj_subtype_prod_id"] ?>" checked>
                    </div>
              <?php } 
              elseif($row["thj_subtype_status_P"] == '0'){ ?>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="sta<?php echo $row["thj_subtype_prod_id"] ?>" >
                    </div>
              <?php } ?>
            </div>
          </td>
          <td><a href="" data-bs-toggle="modal" data-bs-target="#modalCenter<?php echo $row["thj_subtype_prod_id"] ?>"><i class="fa-solid fa-eye text-success"></i></a></td>
          <td><a href="./admin.php?pages=product&func=edit_product&prod_id=<?php echo $row["thj_subtype_prod_id"] ?>"><i class="fa-solid fa-pen-to-square text-primary"></i></a></td>
          <!-- <td>  
            <?php if($row["thj_subtype_status_P"] == '1'){ ?>
                <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="sta<?php echo $row["thj_subtype_prod_id"] ?>" checked>
                        <label class="form-check-label" for="defaultCheck1"> ยกเลิกการขาย </label>
                </div>
              <?php } 
              elseif($row["thj_subtype_status_P"] == '0'){ ?>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="sta<?php echo $row["thj_subtype_prod_id"] ?>" >
                        <label class="form-check-label" for="defaultCheck1"> ยกเลิกการขาย </label>
                </div>
            <?php } ?>
            
          </td> -->
          <td><a href="" data-bs-toggle="modal" data-bs-target="#delete<?php echo $row["thj_subtype_prod_id"] ?>"><i class="fa-solid fa-trash text-danger"></i></a></td>
        </tr>
      </tbody>
      <script>
                    $(function() {
                      $('#sta<?php echo $row["thj_subtype_prod_id"] ?>').change(function() {
                        var ch_val = $(this).prop('checked');
                        var rel = <?php echo $row['thj_subtype_prod_id']; ?>;

                        if(ch_val==true){
                          var status = 1;
                        }
                        if(ch_val==false){
                          var status = 0;
                        }
                        $.ajax({
                            url: 'page/product/data/data.php',
                            type: 'POST',
                            data: {id: rel, value: status},
                            // async: false,
                            success: function (data) {
                              console.log(data);
                              // data = JSON.toString(data);
                              }
                          });

                    
                      })
                    })
                </script>
        <div class="modal fade" id="delete<?php echo $row['thj_subtype_prod_id'] ?>" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog modal-sm" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="modalCenterTitle"><b>ยืนยันการลบ</b></h5>
                  <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div class="modal-body" align="center">
                    <div class="row" >
                          <i class="fa-solid fa-circle-xmark text-danger" style="font-size: 5rem;"></i>
                          <h4 class="mt-3"> คุณต้องการลบใช่หรือไม่ ?</h4>
                    </div>
                </div>
                <div class="modal-footer">
                  <form action="page/product/data/de_prod.php" method="post">
                    <input type="text" name="thj_subtype_prod_id" id="" value="<?php echo $row["thj_subtype_prod_id"] ?>" hidden>
                    <button type="submit" class="btn btn-outline-danger">ลบ</button>
                  </form>
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal"> ยกเลิก </button>
                </div>
              </div>
            </div>
        </div>
      <?php } ?>
    </table>
</div>
<!--/ Basic Bootstrap Table -->
        </div> 
       <?php include('detail_product.php'); ?>

                      