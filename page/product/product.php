<?php

    $like="";
    $type="";
    if(isset($_GET["name_pord"])){
      $name_pord=$_GET["name_pord"];
      $like="AND thj_subtype_prod.thj_subtype_prod_name LIKE '%".$name_pord."%' ";
    }
    else{
      $type="";
      $like=""; 
    }

    $query=mysqli_query($conn,"SELECT COUNT(thj_subtype_prod_id) FROM `thj_subtype_prod`
    LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
    LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
    LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
    LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
    LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
     where thj_account.thj_acc_id=".$row_user["thj_acc_id"]." ".$like."  ");
    $row = mysqli_fetch_row($query);
    
    $rows = $row[0];
    
    $page_rows = 20;  //จำนวนข้อมูลที่ต้องการให้แสดงใน 1 หน้า  ตย. 10 record / หน้า 
    
    $last = ceil($rows/$page_rows);
    
    if($last < 1){
      $last = 1;
    }
    
    $pagenum = 1;
    
    if(isset($_GET['pn'])){
      $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
    }
    
    if ($pagenum < 1) {
      $pagenum = 1;
    }
    else if ($pagenum > $last) {
      $pagenum = $last;
    }
    
    $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
    
    $nquery=mysqli_query($conn,"SELECT *
    FROM thj_subtype_prod
    LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
    LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
    LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
    LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
    LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
    where thj_account.thj_acc_id=".$row_user["thj_acc_id"]." ".$like." 
    ORDER BY thj_subtype_prod.thj_subtype_prod_name $limit");

    $nquery1=mysqli_query($conn,"SELECT *
    FROM thj_subtype_prod
    LEFT JOIN thj_type_product ON thj_subtype_prod.thj_subtype_type_id = thj_type_product.thj_type_prod_id
    LEFT JOIN thj_major ON thj_subtype_prod.thj_product_major_id = thj_major.thj_major_id
    LEFT JOIN thj_account ON thj_major.thj_major_user_id = thj_account.thj_acc_id
    LEFT JOIN thj_type2_product ON thj_subtype_prod.thj_subtype_type2_id = thj_type2_product.thj_type2_product_id
    LEFT JOIN thj_type3_product ON thj_subtype_prod.thj_subtype_type3_id = thj_type3_product.thj_type3_product_id
    LEFT JOIN thj_type4_product ON thj_subtype_prod.thj_subtype_type4_id = thj_type4_product.thj_type4_product_id
    where thj_account.thj_acc_id=".$row_user["thj_acc_id"]." ".$like." 
    ORDER BY thj_subtype_prod.thj_subtype_prod_name $limit");
    
    $paginationCtrls = '';
    
    if($last != 1){
    
    if ($pagenum > 1) {
          $previous = $pagenum - 1;
                  $paginationCtrls .= '
                  <nav aria-label="Page navigation">
                          <ul class="pagination">
                            <li class="page-item prev">
                              <a class="page-link" href="admin.php?page=product&pn='.$previous.'"><i class="tf-icon bx bx-chevron-left"></i></a>
                            </li>
                  ';
          
                  for($i = $pagenum-4; $i < $pagenum; $i++){
                      if($i > 0){
                  $paginationCtrls .= '
                    <li class="page-item">
                        <a class="page-link" href="admin.php?page=product&pn='.$i.'">'.$i.'</a>
                    </li>
                    ';
                      }
              }
          }
          
              $paginationCtrls .= '
              <li class="page-item active">
                <a class="page-link" href="">'.$pagenum.'</a>
             </li>
              ';
          
              for($i = $pagenum+1; $i <= $last; $i++){
                  $paginationCtrls .= '
                    <li class="page-item">
                        <a class="page-link" href="admin.php?page=product&pn='.$i.'">'.$i.'</a>
                    </li>';
                  if($i >= $pagenum+4){
                      break;
                  }
              }
          
          if ($pagenum != $last) {
          $next = $pagenum + 1;
          $paginationCtrls .= '
          <li class="page-item next">
          <a class="page-link" href="admin.php?page=product&pn='.$next.'"><i class="tf-icon bx bx-chevron-right"></i></a>
            </li>
        </ul> </nav>

           ';
          }
              }
?>
                
        <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">หน้าแรก /</span> สินค้า</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
    <div class="card-header d-flex justify-content-between">
        <h4></h4>
        <a href="./admin.php?pages=product&func=insert_product" class="btn btn-dark"><i class="fa-solid fa-plus"></i> เพิ่มสินค้า</a>
    </div>
    
  <form action="./admin.php?pages=product&func=prod_prod" class="form-inline mb-2 ms-2 me-2 " method="get" align="center">
              <input type="text" name="pages" value="product" hidden> 
              <input type="text" name="func" value="prod_prod" hidden> 
              <div class="row">
                  <div class="col-md-12">
                  <div class="input-group mr-2 mb-3 col-3">
                <div class="input-group-prepend">
                    <div class="input-group-text">ชื่อสินค้า</div>
                </div>
                <input type="text" name="name_pord" id="input" class="form-control " placeholder="ชื่อสินค้า">
                <?php 
                  $sql3 = "SELECT thj_subtype_prod_name FROM thj_subtype_prod"; 
                  $result = $conn->query($sql3);  
                  $eventsArr = array();
                  while($row5 = $result->fetch_assoc()){ 
                    array_push($eventsArr, $row5["thj_subtype_prod_name"]); 
                }
                ?>
                  
              </div> 
                  </div>
                  <!-- <div class="col-md-6">
                    <div class="d-flex mb-2">
                            <div class="input-group ">
                              <div class="input-group-prepend">
                                  <div class="input-group-text">ประเภท</div>
                              </div>
                              <select name="type1" id="" class="form-control">
                                  <option value="" selected disabled>เลือก</option>
                                  <option value="2">ปุ๋ย</option>
                                  <option value="1">ข้าว</option>
                                  <option value="3">ยา</option>
                              </select>
                            </div> 
                    </div> 
                  </div> -->
                  <!-- <script type="text/javascript">
                    $(document).ready(function(){
                      $("#type_id").change(function(){
                        var type_id = $(this).val();
                        $.ajax({
                          url: 'page/product/data/data.php',
                          method: 'post',
                          data: {id:type_id,function:'type_id'},
                          success: function(data){
                            console.log(data);
                            $('#type_id2').html(data);
                            // console.log(data);
                          }
                        })
                        })
                        $("#type_id2").change(function(){
                        var type_id2 = $(this).val();
                        $.ajax({
                          url: 'page/product/data/data.php',
                          method: 'post',
                          data: {id:type_id2,function:'type_id2'},
                          success: function(data){
                            console.log(data);
                            $('#type_id3').html(data);
                          }
                        })
                        })
                        $("#type_id3").change(function(){
                        var type_id2 = $(this).val();
                        $.ajax({
                          url: 'page/product/data/data.php',
                          method: 'post',
                          data: {id:type_id2,function:'type_id3'},
                          success: function(data){
                            console.log(data);
                            $('#type_id4').html(data);
                          }
                        })
                        })
                      })
                  </script> -->
                  <!-- <div class="col-md-4">
                    <div class="d-flex mb-2">
                            <div class="input-group ">
                              <div class="input-group-prepend">
                                  <div class="input-group-text">กลุ่ม</div>
                              </div>
                            <select class="select2 form-select" name="type_id" id="type_id">
                                <option selected disabled>--กรุณาเลือกชนิดสินค้า--</option>
                                <?php
                                $sql_tp="SELECT * FROM thj_type_product where thj_type_prod_status != 9";
                                $qry_tp=mysqli_query($conn,$sql_tp);
                                while($row_tp=mysqli_fetch_array($qry_tp)){ ?>
                                    <option value="<?php echo $row_tp['thj_type_prod_id']?>" ><?php echo $row_tp['thj_type_prod_name']?></option>
                                <?php } ?>
                            </select>
                            </div> 
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="d-flex mb-2">
                            <div class="input-group ">
                              <div class="input-group-prepend">
                                  <div class="input-group-text">ประเภท</div>
                              </div>
                              <select name="type_id2" id="type_id2" class="form-control">
                              </select>
                            </div> 
                    </div> 
                  </div>
                  <div class="col-md-4">
                    <div class="d-flex mb-2">
                            <div class="input-group ">
                              <div class="input-group-prepend">
                                  <div class="input-group-text">สินค้า</div>
                              </div>
                              <select name="type_id3" id="type_id3" class="form-control">
                              </select>
                            </div> 
                    </div> 
                  </div> -->
              </div>
              <input type="submit" name="submit" value="ค้นหา" class="btn btn-dark mb-2">
  </form>
<!--/ Basic Bootstrap Table -->
        </div> 
       <?php include('detail_product.php'); ?>

      <div class="constant">
        <a href="./admin.php?pages=product&func=product1&pdtype=3"><button type="button" class="btn btn-outline-danger btn-xl"><i class="fa-solid fa-biohazard"></i>&nbsp;&nbsp; ยา &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='bx bx-chevron-right'></i></button></a>
        <br><br>
        <a href="./admin.php?pages=product&func=product1&pdtype=2"><button type="button" class="btn btn-outline-dark btn-xl"><i class='bx bx-recycle'></i></span>&nbsp;&nbsp; ปุ๋ย &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='bx bx-chevron-right'></i></button></a>
        <br><br>
        <a href="./admin.php?pages=product&func=product1&pdtype=1"><button type="button" class="btn btn-outline-warning btn-xl"><i class='bx bx-spa'></i>&nbsp;&nbsp; เมล็ดพันธุ์ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='bx bx-chevron-right'></i></button></a>
      </div>